import { AmexUxAppPage } from './app.po';

describe('amex-ux-app App', function() {
  let page: AmexUxAppPage;

  beforeEach(() => {
    page = new AmexUxAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
