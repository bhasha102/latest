import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CreateAppService{
private mybaseUrl2: string = "./app/view-user-apps/createapp/productList.json";
private CREATE_APP_URL : string = "./app/view-user-apps/createapp/createApp.json";
constructor(private http : Http){
  }
  getProductList():Observable<Object[]>{
    return this.http.get(this.mybaseUrl2)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
  
    createApp(app):Observable<any> {
    // TODO :: instead of get make use of post method with the "app"" data
    return this.http.get(this.CREATE_APP_URL)
    .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}