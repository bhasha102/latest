import { Component, OnInit  } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { CreateAppService} from './createapp.service';
import {CreateApp} from './createapp';

@Component({
        selector: 'app-view-user-apps',
        templateUrl:'./createapp.component.html',
        styleUrls :  ['./createapp.component.less']
})

export class CreateAppComponent implements OnInit {
   public myForm: FormGroup; 
   public submitted: boolean; 
   public events: any[] = [];
   errorMessage:string;
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   returnVal : any;
   apigeeList : Object[];
   textValue =[];
   isApigeeProd :string = "nonapigee";
   showtext : boolean = true;
   showmultiple : boolean;
   selectOption : any;
   select : any;
   newApigeeList : Array<CreateApp>;
   selectCategory:string='nonapigee';
   selectcategory = [{name:"Select Product",code:"nonapigee"},{name:"Select Platform",code:"realtime"}];
   updatedMsg :boolean = false;
  constructor(private _fb: FormBuilder, private _addproductService : CreateAppService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
             applicationName : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            phoneNumber : ['', [<any>Validators.required]],
            descriptionName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            callbackName : ['', [<any>Validators.required,Validators.pattern("^((https?|ftp)://)+([A-Za-z]+\\.)?[A-Za-z0-9-]+(\\.[a-zA-Z]{1,4}){1,2}(/.*\\?.*)?(/?[a-zA-Z0-9]*)*$")]],
            chooseCategory : [''],
            envName: [''],
            firstName: [''],
            lastName: [''],
            emailName: [''],
            userName: ['']
         
 
        });
       
        
        this.select ="Non-Apigee Product";
            
    }
    selectProduct(apigeeProd){
            
            if("realtime" ==  apigeeProd)
            {
                    this._addproductService.getProductList().subscribe(
                        apigeeList => this.apigeeList  = apigeeList,
                        error =>  {console.log(error)},
                         () =>  {this.convertApigeeList(this.apigeeList)}
                        );
                 
            }
            else
            {
                this.selectCategory = "nonapigee";
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }
 
   removeUpdateMsg()
    {
        this.updatedMsg = false;
    }


    convertApigeeList(list)
    {
        console.log("convertApigeeList");
        this.newApigeeList= [];
        for (var key in list) {
            var value = list[key];
           
           let tempObj:CreateApp ={name:"", author:"", state:false};
            tempObj['name'] = key; 
            tempObj['author'] = value; 
            tempObj['state'] = false;  
            this.newApigeeList.push(tempObj);
        }
        this.selectCategory = "realtime";
        this.showModal =true;
        this.showmultiple = true;
        this.showtext = false;
    }
     save(formValue, isValid)
     {
         this._addproductService.createApp(formValue).subscribe(
             result => this.onSaveResult(result),
             error => this.onSaveError(error)
         );
    }

    onSaveResult(result)
    {
        console.log(result);
        this.updatedMsg = true;
        this.myForm.reset();
        this.submitted = true;
    }
    onSaveError(error)
    {
        console.error(error);
        // TODO :: Let the user know about the error.
    }

   
    displaySelect()
    {

        let j=0;
        this.textValue = [];
        for(let i =0;i< this.newApigeeList.length;i++)
        {
                if(this.newApigeeList[i].state)
                {
                        this.textValue[j] = this.newApigeeList[i].author;
                        j++;
                }
        }
        
        if(j === 0)
        {
                this.isApigeeProd = "nonapigee";
                  this.selectCategory = "nonapigee";
                this.showtext = true;
                this.showmultiple = false;
        }
        else
        {
                this.isApigeeProd = "realtime";
                this.selectCategory = "realtime";
                this.showtext = false;
                this.showmultiple = true;
        }
        this.showModal =false;
    }
    close()
    {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
            this.showModal =false;
            console.log(this.isApigeeProd);
    }

    closed(event){
        console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }
}
    
