import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-view-user-apps',
  templateUrl: './view-user-apps.component.html',
  styleUrls: ['./view-user-apps.component.less']
})
export class ViewUserAppsComponent {

   public constructor(private titleService: Title) { 
            this.setTitle();
                 } 

 pageTitle:string ;

 errorMessage:string;
      
      ngOnInit() {
        
      }
       
        public getTitle()
        {
            this.pageTitle = this.titleService.getTitle();
        }
         
         public setTitle() {
             this.getTitle();       
            this.titleService.setTitle( this.pageTitle);
    }

}  