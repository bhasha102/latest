import { Injectable } from '@angular/core';
//import {MyApps} from './applist';
import { AppVo } from './app.vo';
import { Http, Response, Headers, RequestOptions , URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MAService{
private mybaseUrl: string = "./app/view-user-apps/applist/applist.json";
constructor(private http : Http){
  }

  getapps():Observable<AppVo[]>{
    return this.http.get(this.mybaseUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }        
}