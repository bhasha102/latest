import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {MAService} from './applist.service';
//import {MyApps} from './applist';
import { AppVo } from './app.vo';

@Component({
  selector: 'app-view-user-apps',
  templateUrl: './applist.component.html',
  styleUrls: ['./applist.component.less']
})
export class ApplistComponent implements OnInit {

  public constructor(private titleService: Title, private myappservice: MAService ) { 
            this.setTitle();
                 } 

 pageTitle:string ;
 myapps:AppVo[] ;
 errorMessage:string;
      
      ngOnInit() {
         this.getapp();
      }
       
        public getTitle()
        {
            this.pageTitle = this.titleService.getTitle();
        }
         
         public setTitle() {
             this.getTitle();       
            this.titleService.setTitle( this.pageTitle +" MA" );
    }

    getapp()
    {
        this.myappservice.getapps().subscribe(
                        apps => this.myapps  = apps,
                        error =>  {console.log(error)});
                       
                        
     }
     
}  