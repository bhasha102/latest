import {Injectable }from '@angular/core'; 
import {Http, Response, Headers, RequestOptions }from '@angular/http'; 
import {Observable }from 'rxjs/Rx'; 
import {Platform }from './ext-platform'; 

import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch'; 

@Injectable()
export class External_AddProductService {

  //   private baseUrl_Apigee:string = "./app/ext-admin/products/addProduct/admin-product-apigee.json"; 
  //   private baseUrl_AddProduct:string = "http://localhost:8085/dev-portal-intra/addProdDetails";


 private baseUrl_Apigee:string = "http://localhost:8085/dev-portal-intra/setSchema";
private baseUrl_AddProduct:string = "http://localhost:8085/dev-portal-intra/addProdDetails";

  responseObj:any; 
  headers = new Headers(); 
  constructor(private http:Http) {
  }

  
  getApigeeList(obj:Object):Observable < JSON >  {

    this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods'); 
    this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE'); 
    this.headers.append('Access-Control-Allow-Origin', '*'); 
    //let jsonData = JSON.stringify(formData);
    return this.http.post(this.baseUrl_Apigee, obj,  {headers:this.headers })// ...using post request
.map((res:Response) => res.json())
      .do(data =>  {
        this.responseObj = data; 
      })// ...and calling .json() on the response to return data

      .catch((error:any) => Observable.throw(error || 'Server error')); 

  }

  submitproduct(formData:FormData):Observable < JSON >  {
    this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods'); 
    this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE'); 
    this.headers.append('Access-Control-Allow-Origin', '*'); 
    //let jsonData = JSON.stringify(formData);
    return this.http.post(this.baseUrl_AddProduct, formData,  {headers:this.headers })// ...using post request
.map((res:Response) => res.json())
      .do(data =>  {
        this.responseObj = data; 
      })// ...and calling .json() on the response to return data

      .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any
}



}