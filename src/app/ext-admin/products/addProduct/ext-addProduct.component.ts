import { Component, OnInit  } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { External_AddProductService} from './ext-addProduct.service';
import { Platform} from './ext-platform';
import { ApigeeProduct} from './ext-apigeeProducts';

@Component({
        selector: 'ext-addproduct',
        templateUrl:'./ext-addProduct.component.html',
        styleUrls :  ['./ext-addProduct.component.less']
})

export class External_AddProductComponent implements OnInit {
   public myForm: FormGroup; // our model driven form
   public submitted: boolean; // keep track on whether form is submitted
   public events: any[] = []; // use later to display form changes
   errorMessage:string;
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   file : Blob;
   returnVal : any;
   apigeeList : JSON;
   textValue = [];
   saveList: Array<string> = [];
   isApigeeProd :string = "nonapigee";
   showtext : boolean = true;
   showmultiple : boolean;
   selectOption : any="0";
   select : any;
   platformDetails : Platform[];
   newApigeeList : Array<ApigeeProduct>;
   selectCategory:string='nonapigee';
   selectcategory = [{name:"Non-Apigee Product",code:"nonapigee"},{name:"Link Apigee Product",code:"realtime"}];
   isSuccess :boolean = false;
   isFailure : boolean = false; 
   prodCategory : string = "PRIVATE";
  failureMsg: string ="";
   apigeeListResp = JSON;


  constructor(private _fb: FormBuilder, private _addproductService : External_AddProductService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
            prodDescription : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            prodName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            prodLocation : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload : [''],
            chooseCategory : [''],
            prodCategory :['']
         
 
        });
        this.platformDetails=[];
        this.getApigeeList();
        this.select ="Non-Apigee Product";
            
    }

    getApigeeList(){

var input = new Object();
input['dbSchema']='EXTERNAL';

        this._addproductService.getApigeeList(input).subscribe(
                        apigeeList => {
                            this.apigeeListResp  = apigeeList;
                            if(this.apigeeListResp == null || this.apigeeListResp['oAuthMapExt'] == null)
                            {
                                this.failureMsg = "Failed to fetch Apigee List.";
                                this.isFailure = true;
                                this.isSuccess = false;
                            }
                            else{
                                this.apigeeList = this.apigeeListResp['oAuthMapExt'];
                                this.isFailure = false;
                            }
                            console.log(this.apigeeList);
                            },
                        error =>  {console.log(error)},
                         () =>  {this.convertApigeeList(this.apigeeList)} //this.selectOption = this.platformDetails[2],console.log(this.selectOption)
        );
    }

    isApigeeProduct(apigeeProd){
            console.log("isApigeeProduct func : "+apigeeProd);
            if("realtime" ==  apigeeProd)
            {
                   this.showModal = true;
                    this.showtext = false;
                    this.showmultiple = true;
            }
            else
            {
                this.selectCategory = "nonapigee";
                    console.log(" isApigeeProduct nonapigee");
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }
 
   removeUpdateMsg()
    {
        this.isSuccess = false;
        this.isFailure = false;
    }

    handleInputChange(e){
         this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0]; 
    }

    convertApigeeList(list)
    {
        console.log("convertApigeeList");
        this.newApigeeList= [];
        for (var key in list) {
            var value = list[key];
           
           let tempObj:ApigeeProduct ={name:"", author:"", state:false};
            tempObj['name'] = key; 
            tempObj['author'] = value; 
            tempObj['state'] = false;  
            this.newApigeeList.push(tempObj);
        }
    }
    
    save(formValue, isValid) {
        var input = new FormData();
    
        input.append('prodDescription', formValue.prodDescription);
        input.append('prodName', formValue.prodName);
        input.append('orgIdList', '');
        input.append('prodLocation', formValue.prodLocation);
        if(formValue.chooseCategory == "nonapigee")
        {
            input.append('nonapigeeproducts', "NonApigeeProduct");
        }
        else{
            input.append("apigeeproducts", this.saveList);
        }

        input.append('prodCategory', formValue.prodCategory);
        formValue.uploadFile = this.file;
        input.append('fileupload', formValue.uploadFile);

        console.log("FORM :");
        console.log(formValue.values);
        console.log("Input :");
        console.log(input['orgId']);
        
        this._addproductService.submitproduct(input).subscribe(
            adminPl =>{ 
                this.returnVal = adminPl;
                console.log(this.returnVal);

                    this.isSuccess = true;
                    this.myForm.reset();
                    this.submitted = true;
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
    }

   
    displaySelect()
    {
        let j=0;
        this.textValue = [];
        this.saveList = [];
        for(let i =0;i< this.newApigeeList.length;i++)
        {
                if(this.newApigeeList[i].state)
                {
                    console.log(this.newApigeeList[i].state)
                        this.textValue[j] = this.newApigeeList[i].name;
                        this.saveList[j] = this.newApigeeList[i].name+"^"+this.newApigeeList[i].author; 
                        
                        console.log(this.textValue[j]);
                        j++;
                }
        }
        
        if(j === 0)
        {
                this.isApigeeProd = "nonapigee";
                  this.selectCategory = "nonapigee";
                this.showtext = true;
                this.showmultiple = false;
        }
        else
        {
                this.isApigeeProd = "realtime";
                this.selectCategory = "realtime";
                this.showtext = false;
                this.showmultiple = true;
        }
        this.showModal =false;
    }
    close()
    {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
            this.showModal =false;
            console.log(this.isApigeeProd);
    }

    closeModal(event){
        console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }
}
    
