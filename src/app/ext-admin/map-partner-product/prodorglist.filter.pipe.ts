import { Pipe, PipeTransform } from '@angular/core';
import { MapPartnerProduct } from './mappartnerproduct';

@Pipe({
    name: 'prodorgfilter',
    pure: false
})
export class ProdOrgListFilterPipe implements PipeTransform {

    transform(item: MapPartnerProduct[], productnameFilter: string): any {
        if (productnameFilter === undefined) {
            return item;
        }
        return item.filter(function (qt) {
            if (qt.product_name.toLowerCase().indexOf(productnameFilter.toLowerCase()) !== -1)
                return qt.product_name.toLowerCase().includes(productnameFilter.toLowerCase());
        })
    }
}