import {Injectable }from '@angular/core'; 
import {Http, Response, Headers, RequestOptions, URLSearchParams }from '@angular/http'; 
import {Observable }from 'rxjs/Rx'; 
import {Partner }from './ext-partner'; 
import {UserList }from './ext-userList'; 
import {JsonpModule}from '@angular/http'; 

import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch'; 

@Injectable()
export class External_ManageUserService {

//   private baseUrl_Partners:string = "http://localhost:8085/dev-portal-intra/getOrgList"//'./app/ext-admin/partner.json'; 
//   private baseUrl_UserData:string = "./app/ext-admin/userList.json"; 
//   private baseUrl_Roles:string = "./app/ext-admin/roles.json"; 
//   private baseUrl_UpdateUserRole:string = "http://localhost:8085/dev-portal-intra/updateUserRoles";


private baseUrl_Partners:string = "http://localhost:8085/dev-portal-intra/getOrgList";
private baseUrl_UserData:string = "http://localhost:8085/dev-portal-intra/updateUserData";
private baseUrl_Roles:string = "http://localhost:8085/dev-portal-intra//getRoles";
private baseUrl_UpdateUserRole:string = "http://localhost:8085/dev-portal-intra/updateUserRoles";

  constructor(private http:Http) {
  }

  getPartners():Observable < Partner[] >  {
    const headers = new Headers(); 
	headers.append('Access-Control-Allow-Headers', 'Content-Type'); 
	headers.append('Access-Control-Allow-Methods', 'GET'); 
	headers.append('Access-Control-Allow-Origin', '*'); 
    return this.http.get(this.baseUrl_Partners)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

  getUdetails(orgId:string):Observable < UserList[] >  {
       let params = new URLSearchParams(); 
       params.set('orgId', orgId); 
       params.set('indicator', 'S'); 
       params.set('userId', ''); 
       params.set('status', ''); 
      return this.http.post(this.baseUrl_UserData, params)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error')); 
     
  }

  getRoles():Observable < Object[] >  {
    const headers = new Headers(); 
	    headers.append('Access-Control-Allow-Headers', 'Content-Type'); 
	    headers.append('Access-Control-Allow-Methods', 'GET'); 
	    headers.append('Access-Control-Allow-Origin', '*'); 
      return this.http.get(this.baseUrl_Roles)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error')); 
  }

  changeStatusOrDelete(orgId:string, userId:Array < string > , status:Array < string > , indicator:string):Observable < UserList[] >  {
       let params = new URLSearchParams(); 
       params.set('orgId', orgId); 
       params.set('indicator', indicator); 
       params.set('userId', userId.join(',')); 
       params.set('status', status.join(',')); 
      return this.http.post(this.baseUrl_UserData, params)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error')); 

  }

  updateUserRole(orgId, roleID, roleName, guid, userId):Observable < any >  {
      let params = new URLSearchParams(); 
       params.set('orgId', orgId); 
       params.set('roleId', roleID); 
       params.set('userId', userId); 
       params.set('guid', guid); 
       params.set('roleNm', roleName); 
      return this.http.post(this.baseUrl_UpdateUserRole, params)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error')); 
  }
  
}