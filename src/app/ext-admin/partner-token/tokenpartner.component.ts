import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { TokenPartnerService } from './tokenpartner.service';
import { PList } from './tokenpartner';
import { TokenDetails } from './partnerDetails';
import { Dates } from './dates';

// import { DatePickerModule } from 'ng2-datepicker';
// import { DatePickerOptions  } from 'ng2-datepicker';

// import { MyDatePickerModule } from 'mydatepicker';
// import { IMyOptions } from 'mydatepicker';

@Component({

    selector: 'tokenpartner',
    templateUrl: './tokenpartner.component.html',
    styleUrls: ['./tokenpartner.component.less']
})

export class TokenPartnerComponent implements OnInit {

    tempk: number;
    quo: number;
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changs
    errorMessage: string;
    partnerList: PList[];
    partnerDetails: TokenDetails[];
    success: string;
    error: string;
    showMessage = false;
    showtable = false;
    showField = true;
    showBut = true;
    showQuota = false;
    submitMessage: string;
    successMsg = false;
    updatedMsg: boolean = false;
    showModal: boolean = false;
    showExists:boolean = false;
    showNotExists:boolean = false;
    quotaError:boolean=false;   
    
    startDate = new Date();
    endDate = new Date();

    

//     private DatePickerOptions: DatePickerOptions = {
        
//          minDate: this.startDate,
//          maxDate: this.endDate,
//          format: 'MM-DD-YYYY',
//          autoApply: true,
//          initialDate: this.startDate
// };

    

    // //Functioanlity to get the rangestart for enabling dates
    // startDate = new Date();
    // start_day = this.startDate.getDate() - 1;
    // start_month = (this.startDate.getMonth() + 1);
    // start_year = this.startDate.getFullYear();

    // //Functioanlity to get the rangestart for enabling dates
    // endDate = new Date();
    // end_day = this.endDate.getDate() + 5;
    // end_month = (this.endDate.getMonth() + 1);
    // end_year = this.endDate.getFullYear();


    // //Setting properties of date picker
    // private myDatePickerOptions: IMyOptions = {
    //     todayBtnTxt: 'Today',
    //     dateFormat: 'mm-dd-yyyy',
    //     firstDayOfWeek: 'mo',
    //     sunHighlight: true,
    //     height: '34px',
    //     inline: false,
    //     disableUntil: { year: this.start_year, month: this.start_month, day: this.start_day },
    //     disableSince: { year: this.end_year, month: this.end_month, day: this.end_day },
    //     selectionTxtFontSize: '16px',
    //     showTodayBtn: true,
    //     showClearDateBtn: true,
    //     showSelectorArrow: true,
    //     inputValueRequired : true,
    //     indicateInvalidDate : false,
    //     dayLabels: { su: 'Sun', mo: 'Mon', tu: 'Tue', we: 'Wed', th: 'Thu', fr: 'Fri', sa: 'Sat' },
    //     monthLabels: { 1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec' }
    // };

    constructor(private _fb: FormBuilder,
        private _tokenpartnerService: TokenPartnerService) { }


    ngOnInit() {
        this.myForm = this._fb.group({
            partnerName: [''],
            orgToken: [''],
            tokenQuota: ['', [<any>Validators.required, Validators.maxLength(2)]],
            extUrl: [''],
            date: ''
            //  tokenExpiry: ['', Validators.required]
        });
        this.endDate.setDate(this.endDate.getDate() + 5);
        this.getPartnerList();
    }

    @Output()
    getPartnerList() {
        this._tokenpartnerService.getPartnerList().subscribe(
            partnerDetails => this.partnerList = partnerDetails,
            error => { console.log(error) });
    }
    useridactive(isvisible, orgName) {
        this.showModal = isvisible;
        this.updatedMsg = false;
    }

    closeModal(event) {
        console.log(event.target.className);
        if (event.target.className == "modal") {
            this.showModal = false;
        }
    }

    removeUpdateMsg() {
        this.updatedMsg = false;
    }

    onChange(orgId) {
        this._tokenpartnerService.getPartnerDetails().subscribe(
            UserDetails => {

                this.partnerDetails = UserDetails;
                console.log("partner details");
                console.log(this.partnerDetails);
                if (this.partnerDetails != null) {
                    this.updatePartnerDetails();
                    this.showBut = false;
                    this.showExists = true;
                }
                else {
                    this.showField = false;
                    this.showtable = true;
                    this.showBut = true;
                    this.showNotExists = true;
                    this.showQuota = true;
                }
            },
            error => { console.log(error) },
            () => { }
        );
        this.showMessage = false;
    }

    updatePartnerDetails() {
        this.showtable = true;
        this.myForm.controls['orgToken'].patchValue(this.partnerDetails[0].orgToken);
        this.myForm.controls['extUrl'].patchValue(this.partnerDetails[0].extUrl);
        let tempDate = new Date(+this.partnerDetails[0].tokenExpiry);
        console.log(tempDate);
        this.myForm.controls['tokenQuota'].patchValue(this.partnerDetails[0].tokenQuota);
    }
    keyPress(event: any) {
         console.log("in func",event);

        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        console.log("outer input"+inputChar);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
        else if (!pattern.test(inputChar).valueOf()) {
            event.preventDefault();
        }
    }
    validation(value)
    {
        console.log("VALUE 1: "+value);
         this.quotaError = false;
        if(value>20)
        {
             console.log("VALUE 2: "+value);

             this.quotaError = true;
        }
    }
valuechange(event: any)
{
   console.log("in func",event);
     const pattern = /[0-9\+\-\ ]/;
      let inputChar = String.fromCharCode(event);
      var temp = this.myForm.controls['tokenQuota'].value.concat(inputChar);
        this.tempk = parseInt(temp);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
        else if (!pattern.test(inputChar).valueOf()) {
            event.preventDefault();
        }
        if (this.tempk > 20 ) {
            this.quotaError = true;
        }
        else if(this.tempk < 1)
            {
                  this.quotaError = true;
            }
}
    save(model: any, isValid: boolean) {
        this.updatedMsg = true;
        console.log(model, isValid);
        this.myForm.reset();
        this.submitted = true; // set form submit to true
    }

    copyToClipboard(elementId) {
    var copyDiv = document.getElementById('extUrl');
    copyDiv.focus();
    document.execCommand('SelectAll');
    document.execCommand("Copy", false, null);   
    }
  closeErr(event){
            this.showExists =false;
           this.showNotExists =false;
            
    }
}