import { Component, OnInit  } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { Public_AddProductService} from './public-addProduct.service';
import { Platform } from './public-platform';
import { ApigeeProduct} from './public-apigeeProducts';

@Component({
        selector: 'public-addproduct',
        templateUrl:'./public-addProduct.component.html',
        styleUrls :  ['./public-addProduct.component.less']
})

export class Public_AddProductComponent implements OnInit {
   public myForm: FormGroup; // our model driven form
   public submitted: boolean; // keep track on whether form is submitted
   public events: any[] = []; // use later to display form changes
   errorMessage:string;
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   file : Blob;
   returnVal : any;
   apigeeList : Object[];
   textValue = [];
   saveList: Array<string> = [];
   isApigeeProd :string = "nonapigee";
   showtext : boolean = true;
   showmultiple : boolean;
   selectOption : any;
   select : any;
   platformDetails : Platform[];
   newApigeeList : Array<ApigeeProduct>;
   selectCategory:string='nonapigee';
   selectcategory = [{name:"Non-Apigee Product",code:"nonapigee"},{name:"Link Apigee Product",code:"realtime"}];
   isSuccess :boolean = false;
   isFailure : boolean = false; 
   prodCategory : string = "PRIVATE";
  failureMsg: string ="";


  constructor(private _fb: FormBuilder, private _addproductService : Public_AddProductService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
            prodDescription : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            prodName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            adminProduct : [''],
            prodLocation : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            chooseCategory : [''],
            prodCategory :[''],
            prodOwnerEmail:[],
            prodPRCGroup:[],
            prodApproval:[]
         
 
        });
        this.platformDetails=[];
        this.getAddPlatform();
        this.select ="Non-Apigee Product";
            
    }
    isApigeeProduct(apigeeProd){
             console.log("isApigeeProduct func : "+apigeeProd);
            if("realtime" ==  apigeeProd)
            {
                    console.log("before json call isApigeeProduct");
                    this._addproductService.getApigeeList().subscribe(
                        apigeeList => this.apigeeList  = apigeeList,
                        error =>  {console.log(error)},
                         () =>  {this.convertApigeeList(this.apigeeList)}
                        );
                  console.log("after json call isApigeeProduct");
            }
            else
            {
                this.selectCategory = "nonapigee";
                    console.log(" isApigeeProduct nonapigee");
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }
 
   removeUpdateMsg()
    {
        this.isSuccess = false;
        this.isFailure = false;
    }

    getAddPlatform()
    {
         this._addproductService.getPlatformDetails().subscribe(
                        UserDetails => this.platformDetails = UserDetails,
                        error =>  {console.log(error)}
                          );
    }

    handleInputChange(e){
         this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0]; 
    }

    convertApigeeList(list)
    {
        console.log("convertApigeeList");
        this.newApigeeList= [];
        for (var key in list) {
            var value = list[key];
           
           let tempObj:ApigeeProduct ={name:"", author:"", state:false};
            tempObj['name'] = key; 
            tempObj['author'] = value; 
            tempObj['state'] = false;  
            this.newApigeeList.push(tempObj);
        }
        this.selectCategory = "realtime";
        this.showModal =true;
        this.showmultiple = true;
        this.showtext = false;
    }
    
    save(formValue, isValid) {
        var input = new FormData();
    
        input.append('prodDescription', formValue.prodDescription);
        input.append('prodName', formValue.prodName);
        input.append('orgIdList', formValue.adminProduct);
        input.append('prodLocation', formValue.prodLocation);
        if(formValue.chooseCategory == "nonapigee")
        {
            input.append('nonapigeeproducts', "NonApigeeProduct");
        }
        else{
            input.append("apigeeproducts", this.saveList);
        }

        input.append('prodCategory', formValue.prodCategory);
        formValue.uploadFile = this.file;
        input.append('fileupload', formValue.uploadFile);

        console.log("FORM :");
        console.log(formValue.values);
        console.log("Input :");
        console.log(input['orgId']);
        
        this._addproductService.submitproduct(input).subscribe(
            adminPl =>{ 
                this.returnVal = adminPl;
                console.log(this.returnVal);

                    this.isSuccess = true;
                    this.myForm.reset();
                    this.submitted = true;
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
    }

   
    displaySelect()
    {
        let j=0;
        this.textValue = [];
        this.saveList = [];
        for(let i =0;i< this.newApigeeList.length;i++)
        {
                if(this.newApigeeList[i].state)
                {
                    console.log(this.newApigeeList[i].state)
                        this.textValue[j] = this.newApigeeList[i].name;
                        this.saveList[j] = this.newApigeeList[i].name+"^"+this.newApigeeList[i].author; 
                        
                        console.log(this.textValue[j]);
                        j++;
                }
        }
        
        if(j === 0)
        {
                this.isApigeeProd = "nonapigee";
                  this.selectCategory = "nonapigee";
                this.showtext = true;
                this.showmultiple = false;
        }
        else
        {
                this.isApigeeProd = "realtime";
                this.selectCategory = "realtime";
                this.showtext = false;
                this.showmultiple = true;
        }
        this.showModal =false;
    }
    close()
    {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
            this.showModal =false;
            console.log(this.isApigeeProd);
    }

    closeModal(event){
        console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }
}
    
