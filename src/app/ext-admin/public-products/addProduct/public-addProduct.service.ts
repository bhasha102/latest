import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Platform } from './public-platform';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class Public_AddProductService{

  private baseUrl_Apigee: string = "./app/ext-admin/public-products/addProduct/admin-product-apigee.json";
  private baseUrl_AddProduct: string = "http://localhost:8085/dev-portal-intra/addProdDetails";
  private baseUrl_PlatformDetails: string = "./app/ext-admin/platform.json";


  // private baseUrl_Apigee: string = "./app/admin/product/add-product/admin-product-apigee.json";
  // private baseUrl_AddProduct: string = "http://localhost:8085/dev-portal-intra/addProdDetails";
  // private baseUrl_PlatformDetails: string = "http://localhost:8085/dev-portal-intra/getOrgListById";
  
  responseObj :any;
  headers = new Headers();
  constructor(private http : Http){
  }

  getApigeeList():Observable<Object[]>{
      return this.http.get(this.baseUrl_Apigee)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }

  submitproduct(formData: FormData): Observable<String> {
    console.log("FormData");
    console.log(formData);
    this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
    this.headers.append('Access-Control-Allow-Origin', '*');
    return this.http.post(this.baseUrl_AddProduct, formData, { headers: this.headers }) // ...using post request
      .map((res: Response) => res)
      .do(data => {
        this.responseObj = data;
      }) 
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }  

   getPlatformDetails():Observable<Platform[]>{
    const headers = new Headers();
  	headers.append('Access-Control-Allow-Headers', 'Content-Type');
  	headers.append('Access-Control-Allow-Methods', 'GET');
  	headers.append('Access-Control-Allow-Origin', '*');
      return this.http.get(this.baseUrl_PlatformDetails)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw('Server error'))
  }

}