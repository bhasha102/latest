
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions ,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import {JsonpModule} from '@angular/http';
import { States } from '../states';
import { Partner } from '../partner';
// import { ManageUserService } from '../../../admin/platform/manageuser.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EditPartnerService{
  private baseUrl: string = "./app/ext-admin/partner/countries.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
 // private baseUrl2: string ="./app/admin/partner/edit-partner/partner.json";
  private baseUrl2: string ="http://localhost:8085/dev-portal-intra/getOrgListById";
  constructor(private http : Http){
  }
  getCountryList():Observable<Country[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

   getStatesList():Observable<States[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

  getPartnerDetails(orgId):Observable<Partner[]>{
    const headers = new Headers();
  	headers.append('Access-Control-Allow-Headers', 'Content-Type');
  	headers.append('Access-Control-Allow-Methods', 'GET');
  	headers.append('Access-Control-Allow-Origin', '*');
     let params = new URLSearchParams();
     params.set('orgId',orgId);
      return this.http.post(this.baseUrl2,params)
      // ...and calling .json() on the response to return data
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw('Server error'))
  }

//  getPartnerList():Observable<Partner[]>{
//        const headers = new Headers();
//    	 headers.append('Access-Control-Allow-Headers', 'Content-Type');
//  	 headers.append('Access-Control-Allow-Methods', 'GET');
//   	 headers.append('Access-Control-Allow-Origin', '*');
//        return this.http.get(this.baseUrl2)
//        // ...and calling .json() on the response to return data
//          .map((res:Response) => res.json())
//          .catch((error:any) => Observable.throw('Server error'))
//    }

  saveEditPartner (body: FormData): Observable<string> {
        let bodyString = body; // Stringify payload
        //let headers      = new Headers({ 'Content-Type': 'multipart/form-data' }); // ... Set content type to JSON
        //let options       = new RequestOptions({ headers: headers }); // Create a request option

       // return this.http.post(this.baseUrl, body, options) // ...using post request
        return this.http.get(this.baseUrl2)
                         .map((res:Response) => "success") // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw('Server error')); //...errors if any
  }
  

}