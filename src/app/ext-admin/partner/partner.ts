
export interface Partner
{
    orgName: string,
    phoneNumber : number,
    addressLine1 : string,
    addressLine2 : string,
    country : string,
    state : string,
    city : string,
    zip :number,
    organizationId : number,
    publicGUID : string,
    dateCreated : string,
    orgId : number
}