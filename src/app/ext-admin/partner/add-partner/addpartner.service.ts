
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import {JsonpModule} from '@angular/http';
import { States } from '../states';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AddPartnerService{
  // private baseUrl: string = "./app/admin/partner/add-partner/countries.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  // private baseUrl1: string = "./app/admin/partner/add-partner/states.json";
  private baseUrl: string = 'http://localhost:8085/dev-portal-intra/adminAddOrg';
  private baseUrl1: string = 'http://localhost:8085/dev-portal-intra/getState';
  private baseUrl2: string = 'http://localhost:8085/dev-portal-intra/editOrgDetails';
  private baseUrl3: string = 'http://localhost:8085/dev-portal-intra/getOrgNames';
  
  headers = new Headers();
  constructor(private http : Http){
  }
  getCountryList():Observable<Country[]>{
    this.headers = new Headers();
	this.headers.append('Access-Control-Request-Headers', 'Content-Type,Access-Control-Request-Methods,Access-Control-Allow-Origin');
   this.headers.append('Access-Control-Request-Method', 'GET');
   this.headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    .map((res:Response) => res.json())
     .catch((error:any) => Observable.throw('Server error'))
  }
  getStatesList(countryCode):Observable<States[]>{
    this.headers = new Headers();
this.headers.append('Access-Control-Request-Headers', 'Content-Type,Access-Control-Request-Methods,Access-Control-Allow-Origin');
 this.headers.append('Access-Control-Request-Method', 'GET');
  this.headers.append('Access-Control-Allow-Origin', '*'); 
   let params = new URLSearchParams();
    params.set('countryCode',countryCode);

    return this.http.get(this.baseUrl1,{search :params})
    .map((res:Response) => res.json())
     .catch((error:any) => Observable.throw('Server error'))
  }
  checkPartnerName(input):Observable<JSON>{
    let params = new URLSearchParams();
    params.set('orgName',input);
    return this.http.post(this.baseUrl3,params)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
   submitPartner(form,action) : Observable<JSON>{
     this.headers = new Headers();
     this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
      this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
       this.headers.append('Access-Control-Allow-Origin', '*');
         this.headers.append('Content-Type', 'application/json');
           let jsonData = JSON.stringify({"action": action,"orgdata": form});
            console.log(form);
             console.log(jsonData);
              return this.http.post(this.baseUrl2, jsonData, { headers: this.headers }) // ...using post request
 .map((res: Response) => res.json())
  .catch((error: any) => Observable.throw(error || 'Server error'));
   }
}