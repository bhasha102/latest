import { Injectable } from '@angular/core';
import { PList} from './delete-partner';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DeletePartnerService{
private baseUrl: string ="http://localhost:8085/dev-portal-intra/getOrgList";
constructor(private http : Http){
  }
  
  getPartners():Observable<PList[]>{
       
    const headers = new Headers(); 
	headers.append('Access-Control-Allow-Headers', 'Content-Type'); 
	headers.append('Access-Control-Allow-Methods', 'GET'); 
	headers.append('Access-Control-Allow-Origin', '*'); 
    return this.http.get(this.baseUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
     
  }
    
}