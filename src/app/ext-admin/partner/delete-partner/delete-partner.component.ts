import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { DeletePartnerService } from './delete-partner.service';
import { External_ManageUserService} from '../../manageUsers/ext-manageUsers.service';
import { AddPartnerService } from '../add-partner/addpartner.service';
import { PList} from './delete-partner';
@Component({
  
    selector: 'deletepartner',
    templateUrl: './delete-partner.component.html',
    styleUrls: ['./delete-partner.component.less']
})
export class DeletePartnerComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
     errorMessage:string;
     partner:PList[];
     updatedMsg : boolean = false;
     deletepartner : string = "0";
     errorMsg : boolean = false;
     returnVal : JSON;
      errorString : string = "Please select a Partner.";
       errorClass : boolean = false;
     constructor(private _fb: FormBuilder, private _delpartnerService : DeletePartnerService ,
      private _addpartnerService : AddPartnerService, private _manageUserService : External_ManageUserService) { }
     ngOnInit() {
        this.myForm = this._fb.group({
             deletepartner: ['']
               });
      this.getPartnersList();
     }

           getPartnersList()
         {
        this._delpartnerService.getPartners().subscribe(
                        partnerDetails => this.partner = partnerDetails,
                        error =>  {console.log(error)});
       console.log(this.partner);
        }
   onChange(){
        if (this.deletepartner != undefined || this.deletepartner ==="0")
        {
           console.log(this.updatedMsg);
        this.updatedMsg = false;
        this.errorMsg = false;
        this.errorClass = false;
        }
        
    }
    deleteRowForm(){
        console.log(this.deletepartner);
        if (this.deletepartner == undefined || this.deletepartner === "0" )
        {
            this.errorString = "Please select a Partner.";
             this.errorMsg = true;
            this.updatedMsg = false;
            this.errorClass = true;
        }
        else
        {
            console.log('Nisha');
             let action = "D";
              let orgObj = {"orgId":+this.deletepartner};
               let toRemovePartner = this.deletepartner;
               this._addpartnerService.submitPartner(orgObj,action).subscribe(
                    adminPl =>{ 
                        this.returnVal = adminPl;
                         console.log(this.returnVal);
                          if(this.returnVal['statusCode'] == 300)
                          {
                               console.log("Success");
                                this.updatedMsg = true;
                                 this.myForm.reset();
                                  this.submitted = true;
                                  this.updatedMsg = true;
                                    this.errorMsg = false;
                                     this.errorClass = true;
                                    this.partner = this.partner.filter(function(qt){
                                        if (qt.organizationId != +toRemovePartner)
                                         return qt;
                                    })
                                    this.deletepartner = "0";
        }
        else{
            console.log('Nisha');
             this.errorString = "Failed to delete Partner. Please try after some time.";
              console.log("Failure");
               this.errorMsg = true;
                this.updatedMsg = false;
        }
  },

       error => { console.log(error) },
        () => { console.log(this.returnVal) }
               )
}}
}