export interface UserList {
    guid :string,
    fName :string,
    lName :string,
    emailAddress :string,
	userId : number,
    state : boolean,
    status : string,
    roles : [{
        roleId : number,
        roleName : string
    }]
}

/*
{
	"guid": "857417578666496ba260d421673da1df",
	"fName": "Bhushan",
	"isUserExist": true,
	"organization": null,
	"isProduct": true,
	"isUser": true,
	"lName": "Dasare",
	"userId": "801",
	"status": "N",
	"emailAddress": "Bhushan.Dasare@aexp.com",
	"roleId": null,
	"roleName": null,
	"roles": [{
		"roleName": "ADMIN",
		"roleId": 1
        }]
    }


*/