import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProduct } from './adminproduct';
import { PA_EditProductService } from './pa-editProduct.service';
import { Platform } from '../../../admin/platform/platform';
import { AdminProductSelect } from './adminproductselect';
import { ModelLocator } from '../../../modelLocator';

@Component({
    selector: 'editProduct',
    templateUrl: './pa-editProduct.component.html',
    styleUrls: ['./pa-editProduct.component.less']
})

export class PA_EditProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    editPRO: AdminProduct;
    showModal: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    file: Blob;
    returnVal: any;
    textValue = [];
    saveList: Array<string> = [];
    platformDetails: Platform[];
    showtext: boolean = true;
    showmultiple: boolean;
    selectOption: any;
    select: any;
    listPRO: AdminProductSelect[];
    categoryUpdate: string = "PRIVATE";
    prodCategory: string = "PRIVATE";
    fileUpdated: string;
    apProdList: Array<string> = [];
    isApigeeProd: string = "nonapigee";
    updatedPlatformId: string;
    selectPlatform: string;
    selectCategory: string = 'nonapigee';
    updatedMsg: boolean = false;
    category = [{ name: "Non-Apigee Product", code: "nonapigee" }, { name: "Link Apigee Product", code: "realtime" }];
    isError: boolean = false;
    errorMsg: string;
    selectedProduct: string = "0";

    _model = ModelLocator.getInstance();

    constructor(private _fb: FormBuilder, private _editproductService: PA_EditProductService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            prodDescription: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            prodName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            editProduct: ['', [<any>Validators.required]],
            prodLocation: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload: [''],
            chooseCategory: [''],
            platform: [''],
            prodCategory: ['']
        });

        this.getDetails();
        this.select = "Non-Apigee Product";

    }

    isApigeeProduct(apigeeProd) {
        console.log(apigeeProd)
        if ("realtime" == apigeeProd) {
            this.selectCategory = this.category[1].code;
            this.showmultiple = true;
            this.showtext = false;
        }
        else {
            this.selectCategory = this.category[0].code;
            this.showtext = true;
            this.showmultiple = false;
        }
    }

    getDetails() {
        this._editproductService.getPlatforms().subscribe(
            UserDetails => this.platformDetails = UserDetails,
            error => { console.log(error) }
        );

        this._editproductService.getAdminProductList().subscribe(
            adminPRO => this.listPRO = adminPRO,
            error => { console.log(error) });

    }
    onChange(prodId) {
        // Need to pass orgId in service
        this._editproductService.getAdminProductDetails(prodId).subscribe(
            adminPRO => {
                this.editPRO = adminPRO;
                this.updateProductDetails();
            },
            error => { console.log(error) },
            () => { }
        );
    }

    updateProductDetails() {
        console.log("editPro");
        console.log(this.editPRO);
        this.myForm.controls['prodName'].patchValue(this.editPRO.prodName);
        this.myForm.controls['prodDescription'].patchValue(this.editPRO.prodDs);
        this.myForm.controls['prodLocation'].patchValue(this.editPRO.prodLocTx);
        if (this.editPRO.apProdList[0] === 'NonApigeeProduct') {
            this.showtext = true;
            this.showmultiple = false;
            this.isApigeeProd = "nonapigee";
            this.selectCategory = this.category[0].code;
        }
        else {
            this.showmultiple = true;
            this.showtext = false;
            this.apProdList = this.editPRO.apProdList;
            this.isApigeeProd = "realtime";
            this.selectCategory = this.category[1].code;//"realtime";
            console.log("thirdtime" + this.selectCategory);
        }
        this.prodCategory = this.editPRO.pvtCtgyId.trim();
        this.fileUpdated = this.editPRO.prodName + ".json";
        this.selectPlatform = this.editPRO.orgId.toString();
    }

    handleInputChange(e) {
        console.log("change");
        this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        console.log(this.file);
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

    save(formValue, isValid) {
        console.log(formValue);
        var input = new FormData();
        input.append('editProdId', this.editPRO.prodId);
        input.append('editProdDescription', formValue.prodDescription);
        input.append('editProdName', formValue.prodName);
        input.append('editProdOrgId', formValue.platform.toString());
        input.append('editProdLocation', formValue.prodLocation);
        input.append('editPrevProdName', this.editPRO.prodName);

        if (formValue.chooseCategory == "nonapigee") {
            input.append('editNonApigeeProducts', "NonApigeeProduct");
        }
        else {
            input.append("editProdApigeeId", this.textValue);
        }

        input.append('editProdCategory', formValue.prodCategory);
        formValue.uploadFile = this.file;
        input.append('fileupload', formValue.uploadFile);
        console.log(formValue);
        this._editproductService.saveEditProductDetails(input).subscribe(
            adminPl => {
            this.returnVal = adminPl
                console.log(this.returnVal);
                if (this.returnVal['prodId'] != 0) {
                    this.updatedMsg = true;
                    this.isError = false;
                    this.submitted = true;
                }
                else {
                    this.updatedMsg = false;
                    this.isError = true;
                    this.errorMsg = "Failed to update Product. Kindly try after some time."
                }
            },
            error => { console.log(error) },
            () => { console.log(this.returnVal) })
    }

    removeUpdateMsg() {
        this.updatedMsg = false;
        this.isError = false
    }

    displaySelect() {

    }
}
