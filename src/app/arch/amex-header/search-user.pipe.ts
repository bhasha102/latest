import { Pipe, PipeTransform } from '@angular/core';
import { Injectable } from '@angular/core';

@Pipe({
  name: 'searchUser'
})
@Injectable()
export class SearchUserPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(args === undefined) return value;
    return value.filter(function(val){
      console.info(val);
      return val.userName.toLowerCase().includes(args.toLowerCase());
    })
  }

}
