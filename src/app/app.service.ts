import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AppService{

private baseUrl: string ="http://localhost:8085/dev-portal-intra/";
 headers = new Headers();
constructor(private http : Http){}

    getAppRole(guid)
    {
        this.headers.append('Access-Control-Request-Headers', 'Content-Type,GUID,Access-Control-Request-Methods,Access-Control-Allow-Origin');
	    this.headers.append('Access-Control-Request-Method', 'GET');
	    this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('GUID',guid);
        return this.http.get(this.baseUrl,{ headers: this.headers }) // ...using post request
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
}