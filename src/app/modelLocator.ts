export class ModelLocator {
    private static instance: ModelLocator;
    //Assign "new Singleton()" here to avoid lazy initialisation

    constructor() {
        if (ModelLocator.instance) {
            return ModelLocator.instance;
        }

        ModelLocator.instance = this;
    }

    static getInstance(): ModelLocator {
        ModelLocator.instance = ModelLocator.instance || new ModelLocator();
        return ModelLocator.instance;
    }

public isLoaderVisible:boolean = false;
}