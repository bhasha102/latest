import { Pipe, PipeTransform } from '@angular/core';
import { ViewAllUsers } from './view-all-users';

@Pipe({
    name: 'userfilter',
    pure: false
})
export class ViewAllUsersPipe implements PipeTransform {

    transform(question: ViewAllUsers[], nameFilter: string): any {
        let tempStatus: Array<String> = ['ACTIVE', 'INACTIVE'];
        if (nameFilter === undefined) {
            console.log("nameFilter : " + nameFilter);
            return question;
        }

        return question.filter(function(qt) {
            if (tempStatus[0].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1 && qt.status.toLowerCase().indexOf('y') !== -1) {
                return qt.status.toLowerCase().includes('y');
            }
            if (tempStatus[1].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1 && qt.status.toLowerCase().indexOf('n') !== -1) {
                return qt.status.toLowerCase().includes('n');
            }
            if (qt.organization.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.organization.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.fName.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.fName.toString().toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.lName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.lName.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.emailAddress.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.emailAddress.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.userId.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.userId.toString().toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.roleName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.roleName.toLowerCase().includes(nameFilter.toLowerCase());
        })

    }
}