import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ViewAllUsers } from './view-all-users';
import { JsonpModule } from '@angular/http';
import { Component, EventEmitter, Input, OnChanges, OnInit, ViewEncapsulation, ElementRef, Output } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ViewAllUsersService {
  private baseUrl: string = "http://localhost:8085/dev-portal-intra/getAllUserData"//"./app/admin/viewAllUsers/view-all-users.json";
  public responseObj: any;

  constructor(private http: Http) {
  }
  getViewAllUsers(): Observable<ViewAllUsers[]> {
    const headers = new Headers();
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET');
    headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

}