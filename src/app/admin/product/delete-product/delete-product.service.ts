import { Injectable } from '@angular/core';
import { ProdList } from './delete-product';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DeleteProductService {
  private baseUrl: string = "http://localhost:8085/dev-portal-intra/getProdList";//"./app/admin/product/delete-product/delete-product.json";
  private baseUrl1: string = "http://localhost:8085/dev-portal-intra/deleteProdDetails";
  constructor(private http: Http) {
  }

  getProducts(): Observable<ProdList[]> {

    return this.http.get(this.baseUrl)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  deleteProduct(prodId, prodName): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Access-Control-Request-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin', 'Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE' });//... Set content type to JSON
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({ "product_ID": prodId, "product_name": prodName });
    return this.http.post(this.baseUrl1, body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}