import { Component, OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { AdminProduct} from './adminproduct';
import { AdminEditProductService} from './editproduct.service';
import { AdminAddProductService} from '../add-product/addproduct.service';
import { Platform } from '../../platform/platform';
import { EditPlatformService } from '../../platform/edit-platform/editplatform.service';
import { ManageUserService} from '../../platform/manageuser.service';
import { AdminProductSelect} from './adminproductselect';
@Component({
        selector: 'editProduct',
        templateUrl:'./editproduct.component.html',
        styleUrls :  ['./editproduct.component.less']
})

export class AdminEditProductComponent implements OnInit {
   public myForm: FormGroup; // our model driven form
   public submitted: boolean; // keep track on whether form is submitted
   public events: any[] = []; // use later to display form changes
   errorMessage:string;
   editPRO : AdminProduct;
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   file : Blob;
   returnVal : JSON;
   textValue =[];
   saveList: Array<string> = [];
   platformDetails : Platform[];
   showtext : boolean =true;
   showmultiple : boolean;
   selectOption : any ;
   select : any;
   listPRO : AdminProductSelect[];
   categoryUpdate : string = "PRIVATE";
   prodCategory : string = "PRIVATE";
   fileUpdated:string;
   apProdList : Array<string> = [];
   isApigeeProd :string = "nonapigee";
   updatedPlatformId :string;
   selectPlatform : string = "0";
   selectCategory:string='nonapigee';
   updatedMsg :boolean = false;
   category = [{name:"Non-Apigee Product",code:"nonapigee"},{name:"Link Apigee Product",code:"realtime"}];
   isError : boolean=false;
   errorMsg : string;
   selectedProduct : string = "0";
   selectedApigeeProd : Array<string>;
   productNameResp = JSON;
   isNamenotAvailable = false;

  constructor(private _fb: FormBuilder, private _editproductService : AdminEditProductService, private _addplatformService : EditPlatformService, private _manageUserService : ManageUserService, private _addProductService : AdminAddProductService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
            prodDescription : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            prodName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9\-][a-zA-Z0-9 \-]*")]],
            editProduct : ['',  Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodLocation : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload : [''],
            chooseCategory : [''],
            platform :['', Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodCategory:['']
        });
      
        this.getDetails();
       // this.selectOption = this.addPl[0];
      //  console.log(this.selectOption)
        this.select ="Non-Apigee Product";
 
    }
     validateDefaultProduct(fieldControl: FormControl){
        //console.log("selectOption" + this.selectOption);
         if(fieldControl.value != undefined && fieldControl.value != null)
        {
            return fieldControl.value === '0' ? {defaultError : "error"} : null;
        }
        else
        {
            return null;
        }
    }

    checkProductName(){
        console.log(this.myForm.controls['prodName'].value);
        let input = this.myForm.controls['prodName'].value;
        this._addProductService.checkProdctName(input).subscribe(
        prodName => {
            this.productNameResp = prodName;
            console.log(this.productNameResp);
            if(this.productNameResp['result'] == "exists")
            {
                this.isNamenotAvailable = true;
            }
            else{
                this.isNamenotAvailable = false;
            }
        },
        error =>{console.log(error)}

        )
    }

    isApigeeProduct(apigeeProd){
            console.log(apigeeProd)
            if("realtime" ==  apigeeProd)
            {
                  this.selectCategory = this.category[1].code;
                  // this.selectCategory = "realtime";
                   this.showmultiple = true;
                   this.showtext = false;
            }
            else
            {
                   this.selectCategory = this.category[0].code;
                   // this.selectCategory = "nonapigee";
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }

    getDetails()
    {
               this._manageUserService.getPlatforms().subscribe(
                        UserDetails => this.platformDetails = UserDetails,
                        error =>  {console.log(error)}
                          );
                        
       this._editproductService. getAdminProductList().subscribe(
                        adminPRO => this.listPRO  = adminPRO,
                        error =>  {console.log(error)});
                        
    }
     onChange(prodId)
     {
            // Need to pass orgId in service
             if (prodId =="0")
            {
                  console.log(prodId)
                  this.myForm.reset();
                  this.showtext = true;
                  this.showmultiple = false;
                  this.selectCategory = "nonapigee";
                  this.selectedProduct = "0";
                  this.selectPlatform  = "0";
                  this.prodCategory = "PRIVATE";
                  this.fileUpdated = "";
            }
            else
            { 
            this._editproductService.getAdminProductDetails(prodId).subscribe(
                        adminPRO => {
                            this.editPRO  = adminPRO;
                            this.updateProductDetails();
                        },
                        error =>  {console.log(error)},
                        () => {}         
            );
            }
           // this.showMessage =false;

     //   this._addplatformService.getUserDetails(plat).subscribe(
    //        response => this.responseObj = response,
     //       error => {console.log(error)}
     //   );
     }

     updateProductDetails()
     {
         console.log("editPro");
         console.log(this.editPRO);
         //this.myForm.patchValue({'city':this.platformDetails[0].city},{'orgName': this.platformDetails[0].orgName});
         this.myForm.controls['prodName'].patchValue(this.editPRO.prodName);
         this.myForm.controls['prodDescription'].patchValue(this.editPRO.prodDs);
         this.myForm.controls['prodLocation'].patchValue(this.editPRO.prodLocTx);
        // this.myForm.controls['prodCategory'].patchValue(this.editPRO.pvtCtgyId);
         if(this.editPRO.apProdList[0] === 'NonApigeeProduct')
         {
                this.showtext = true;
                this.showmultiple = false;
                this.isApigeeProd = "nonapigee";
                //this.selectCategory = "nonapigee";
                this.selectCategory = this.category[0].code;
         }
         else{
                this.showmultiple = true;
                this.showtext = false; 
                this.apProdList = this.editPRO.apProdList;
                this.selectedApigeeProd = this.apProdList;
                this.isApigeeProd = "realtime";
                this.selectCategory = this.category[1].code;//"realtime";
                console.log("thirdtime" +this.selectCategory);
         }
        this.prodCategory = this.editPRO.pvtCtgyId.trim();
        this.fileUpdated = this.editPRO.prodName+".json";
        this.selectPlatform = this.editPRO.orgId.toString();
     }

    handleInputChange(e){
         console.log("change");
         this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];        
         console.log(this.file);
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

    save(formValue, isValid)
    {
        if(this.isNamenotAvailable){
            return false;
        }
        // console.log(formValue);
        var input = new FormData();
        input.append('editProdId',this.editPRO.prodId);
        input.append('editProdDescription', formValue.prodDescription);
        input.append('editProdName', formValue.prodName);
        input.append('editProdOrgId',formValue.platform.toString());
        input.append('editProdLocation', formValue.prodLocation);
        input.append('editPrevProdName', this.editPRO.prodName);
        
        //input.append('selectCategory', formValue.selectCategory);
        if(formValue.chooseCategory == "nonapigee")
        {
            input.append('editNonApigeeProducts', "NonApigeeProduct");
            //input.append("apigeeproducts", '');
        }
        else{
            //input.append('nonapigeeproducts', "");
            console.log("editProdApigeeId   "+this.selectedApigeeProd);
            console.log("editProdApigeeId   0 : "+this.selectedApigeeProd[0]);
            input.append("editProdApigeeId", this.selectedApigeeProd);
        }

        input.append('editProdCategory', formValue.prodCategory);
        formValue.uploadFile = this.file;
        input.append('fileupload', formValue.uploadFile);
        //formValue.uploadFile = this.file;
        //console.log(formValue.uploadFile);
        console.log(formValue);
        this._editproductService.saveEditProductDetails(input).subscribe(
                adminPl =>{ this.returnVal  = adminPl
                    console.log(this.returnVal);
                    if(this.returnVal[0]['prodId'] != 0)
                    {
                        if(this.returnVal[0]['product_desc'] != null)
                        {
                            this.updatedMsg = false;
                            this.isError = true;
                            this.errorMsg = "JSON file is invalid.Kindly upload valid JSON file."
                        }
                        else{
                            this.updatedMsg = true;
                            this.isError = false;
                            //this.myForm.reset();
                            this.submitted = true;
                        }
                        
                    }
                    else{
                        this.updatedMsg = false;
                        this.isError = true;
                        this.errorMsg = "Failed to update Product. Kindly try after some time."
                    }
                },
                error =>  {console.log(error)},
                () => {console.log(this.returnVal)})
               
    }
     removeUpdateMsg()
    {
        this.updatedMsg = false;
        this.isError=false
    }
  /* selectApigeeProducts(selected){
       console.log(selected);
       let options = selected.options;
       console.log(options);
       for(let i=0;i<options.length;i++)
       {
           console.log("In for");
           console.log(options[i].selected);
           console.log(options[i].value);
       }
    //    this.selectedApigeeProd = Array.apply(null,options)  // convert to real Array
    //         .filter(option => option.selectedIndex)
    //         .map(option => {option.value;
    //              console.log("CHANGE"+this.selectedApigeeProd);
    //         })
           
}*/



    displaySelect()
    {
     
    }
}
