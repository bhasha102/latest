import { Pipe, PipeTransform } from '@angular/core';
import { Platform } from '../platform/platform';

@Pipe({
    name: 'mapplatformproductfilter',
})
export class MapFilterPipe implements PipeTransform {

    transform(list: Platform[], nameFilter: string): any {
        if (nameFilter === undefined) {
            return list;
        }

        return list.filter(function (qt) {
            if (qt.orgName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.orgName.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.organizationId.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.organizationId.toString().toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.dateCreated.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.dateCreated.toLowerCase().includes(nameFilter.toLowerCase());
        })
    }
}