import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { MapPlatformProduct } from './mapplatformproduct';
import { JsonpModule } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MapPlatformProductService {
  private baseUrl: string = "http://localhost:8085/dev-portal-intra/mapOrgProducts";
  private baseUrl1: string = "http://localhost:8085/dev-portal-intra/mappingOrgMulProd";
  private baseUrl12: string = "./app/admin/map-platform-product/mapplatformproduct.json";
  public responseObj: JSON;
  constructor(private http: Http) {
    this.responseObj = null;
  }

  getMapProductDetails(organizationId: string): Observable<JSON> {
    let bodyString = new URLSearchParams();
    bodyString.set('organizationId', organizationId);  // Set URL
    return this.http.post(this.baseUrl, bodyString) // ...using post request
      .map((res: Response) => res.json())
      .do(data => {
        this.responseObj = data;
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any

  }

  addMapProductDetails(prodId: Array<string>, Indicator: string): Observable<JSON> {
    let bodyString = new URLSearchParams();
    bodyString.set('prodID', prodId.join(','));
    bodyString.set('indicator', Indicator);
    return this.http.post(this.baseUrl1, bodyString) // ...using post request
      .map((res: Response) => res.json())
      .do(data => {
        this.responseObj = data;
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any

  }
}