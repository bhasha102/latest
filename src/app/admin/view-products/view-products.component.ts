import { Component, OnInit } from '@angular/core';
import { ViewProductsService } from './view-products.service';
import { ViewProducts} from './view-product';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
//import { ModelLocator } from '../../modelLocator';

declare let jsPDF;

@Component({
  selector: 'view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.less']
})

export class ViewProductsComponent implements OnInit {
  allProducts:ViewProducts[];
  sortVal : string;
  sortType : string = 'Asc';
  showModal : boolean = false;
  private timer;
  ticks=0;
  showLoader : boolean = false;
  previousKey:[''];
 // _model = ModelLocator.getInstance();

  constructor( private _viewproductService : ViewProductsService, private router:Router){
  }

  ngOnInit(){
    //this._model.isLoaderVisible = true;
     this.getAllProducts(); //Getting all products
  }

  //Function to get All Products
  getAllProducts(){
    //this._model.isLoaderVisible = true;  
    this._viewproductService.getAllProducts().subscribe(
        allProducts => this.allProducts = allProducts,
        error =>  {console.log(error)},
        //() => {this._model.isLoaderVisible = false; }
    );  
  }

  //Functionality for sorting 
  sort(type, key) {
        if (this.previousKey != key) {
            this.sortType = 'Asc';
            type = 'Asc';
        }
        if (type == 'Asc') {
            this.sortVal = key;
            this.sortType = 'Desc';
        } else if (type == 'Desc') {
            this.sortVal = "-" + key;
            this.sortType = 'Asc';
        }
        this.previousKey = key;
    }

  //Functionality to Copy to Clipboard
  copyToClipboard(elementId){
     var textarea = document.createElement("textarea");
     textarea.textContent = document.getElementById(elementId).innerText;
     textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
     document.body.appendChild(textarea);
     textarea.select()
     try {
        document.execCommand("copy");  // Security exception may be thrown by some browsers.
     } catch (ex) {
        console.warn("Copy to clipboard failed.", ex);
        return false;
     } finally {
        this.successMessage(true);
        document.body.removeChild(textarea);
    }
  }

  //Showing success message based on timer
  successMessage(isvisible){
    this.timer = Observable.interval(1000).take(3);
    this.timer.subscribe(t => this.tickerFunc(t));
    this.showModal =isvisible;
  }

  tickerFunc(tick){
    this.ticks = tick
    if(this.ticks >= 2)
    {
        this.showModal =false;
        this.ticks= 0;
    }
    return false;
  }

  // Functionality to generate PDF
  generatePDF(compId) {
    var pdfsize = 'a4';
    var pdf = new jsPDF('p', 'pt', pdfsize);
    
    var header = function (data) {
    pdf.setFontSize(15);
    pdf.setTextColor(40);
    pdf.setFontStyle('normal');
    pdf.text("API-Dev Portal", data.settings.margin.left + 170,55);
    };

    var options = {
    beforePageContent: header,
    margin: {right: 90,top:70,left:90},

      styles: {
          overflow: 'linebreak',
          fontSize: 10,
          rowHeight: 17,
          columnWidth: 'auto',
          textColor: '#000000'
      },
    
      createdHeaderCell: function (cell, data) {
          if (cell.raw == 'Product' || cell.raw == 'Platform' || cell.raw == 'Date Created') {//paint.Name header red
            cell.styles.fontSize= 11;
            cell.styles.textColor = '#ffffff';
            cell.styles.fillColor =  [52, 73, 94];
            cell.styles.rowHeight =  18;
            cell.styles.halign = 'center';
          } 
          else {
            cell.styles.textColor = 255;
            cell.styles.fontSize = 10;
            cell.styles.halign = 'center';
          }
         },
     columnStyles: {
      1: {columnWidth: 'auto'}
     }
    };

    var res = pdf.autoTableHtmlToJson(document.getElementById(compId));
    pdf.i
    pdf.autoTable(res.columns, res.data, options);
    pdf.save("API-Dev Portal.pdf");
  };

  // Functionality to download CSV
  download(){
      let newUser :Array<Object> =[];
        this.allProducts.forEach(element => {
            let tempObj = {};
            tempObj['PRODUCT'] = element['product_name'];
            tempObj['PLATFORM'] = element['orgName'];
            tempObj['DATE CREATED'] = element['dateCreated'];
            newUser.push(tempObj);
        });
      
        var csvData = this.ConvertToCSV(newUser);
        var a = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        var blob = new Blob([csvData], { "type": 'text/csv;charset=utf-8;' });
        //var blob = new Blob([csvData], { type: 'text/csv' });
        var url= window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'API-Dev Portal.csv';
        a.click();
    } 

  ConvertToCSV(objArray):string {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
    var row = "";
    for (var index in objArray[0]) {
        //Now convert each value to string and comma-separated
         row += index + ',';
    }
    row = row.slice(0, -1);
     //append Label row with line break
    str += row + '\r\n';   
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
         if (line != '') line += ','
         line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
  }

  //Functionality for Print
  printDiv(eleId) {
    var printContents = document.getElementById(eleId).outerHTML;
    var originalContents = document.body.innerHTML;    

    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.2px solid #A3A2A2;' +
        'padding;0.5em;' +
        '}' +
        '</style>';
    htmlToPrint += printContents;
    var newWin=window.open(); 
    newWin.document.write('<!DOCTYPE html><html><body><h1>API-Dev Portal</h1><br><div>'+ htmlToPrint +
    '</div></body></html>');
    newWin.print();
    newWin.onfocus=function(){ newWin.close();}
    return true;
  }
}


