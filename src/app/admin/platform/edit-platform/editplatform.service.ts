
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JsonpModule } from '@angular/http';
import { Platform } from '../platform';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EditPlatformService {
  //private baseUrl2: string ="./app/admin/platform/admin.json";
  private baseUrl2: string = "http://localhost:8085/dev-portal-intra/getOrgListById";
  constructor(private http: Http) {
  }
  getPlatformDetails(orgId): Observable<Platform[]> {
    const headers = new Headers();
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET');
    headers.append('Access-Control-Allow-Origin', '*');
    let params = new URLSearchParams();
    params.set('orgId', orgId);
    return this.http.post(this.baseUrl2, params)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

}