import { Injectable } from '@angular/core';
import { Platform } from './platform';
import { UserList } from './user-list';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ManageUserService {
    //private baseUrl2: string = 'http://localhost:8085/dev-portal-intra/getUserData';
    /*private baseUrl: string ="./app/admin/platform/admin.json";
    private baseUrl1: string ="./app/admin/platform/user-list.json";
    private baseUrl2: string ="./app/admin/platform/roles.json";
    */
    private baseUrl: string = "http://localhost:8085/dev-portal-intra/getOrgList";
    private baseUrl1: string = "http://localhost:8085/dev-portal-intra/updateUserData";
    private baseUrl2: string = "http://localhost:8085/dev-portal-intra//getRoles";
    private baseUrl3: string = "http://localhost:8085/dev-portal-intra/updateUserRoles";
    private baseUrl4: string = "http://localhost:8085/dev-portal-intra/getAdsUserDetails";
    private baseUrl5: string = "http://localhost:8085/dev-portal-intra/addAdsUserDetails";
    headers = new Headers();

    constructor(private http: Http) { }

    getPlatforms(): Observable<Platform[]> {

        return this.http.get(this.baseUrl)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    getUdetails(orgId: string): Observable<UserList[]> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('indicator', 'S');
        params.set('userId', '');
        params.set('status', '');
        return this.http.post(this.baseUrl1, params)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    getRoles(): Observable<Object[]> {
        return this.http.get(this.baseUrl2)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    changeStatusOrDelete(orgId: string, userId: Array<string>, status: Array<string>, indicator: string): Observable<UserList[]> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('indicator', indicator);
        params.set('userId', userId.join(','));
        params.set('status', status.join(','));
        return this.http.post(this.baseUrl1, params)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    updateUserRole(orgId, roleID, roleName, guid, userId): Observable<any> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('roleId', roleID);
        params.set('userId', userId);
        params.set('guid', guid);
        params.set('roleNm', roleName);
        return this.http.post(this.baseUrl3, params)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }
    getUserDetails(adsId: string): Observable<JSON> {
        let params = new URLSearchParams();
        params.set('adsId', adsId);
        return this.http.post(this.baseUrl4, params)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    addUser(userData: Object): Observable<JSON> {
        this.headers = new Headers();
        this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
        this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Content-Type', 'application/json');
        return this.http.post(this.baseUrl5, userData)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

}