import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { Title } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { FaqComponent } from './faq/faq.component';
import { AmexHeaderComponent } from './arch/amex-header/amex-header.component';
import { AmexFooterComponent } from './arch/amex-footer/amex-footer.component';
import { OrderByPipe } from './arch/amex-header/order-by.pipe';

import { AdminComponent } from './admin/admin.component';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductService } from './product/product-list/product-list.service';
import { ProductDetailComponent } from './product/product-details/product-detail.component';
import { ApiReferenceComponent } from './product/product-details/api-reference.component';
import { ProductDetailService } from './product/product-details/product-detail.service';
import { ProdApiDocComponent } from './prod-api-doc/prod-api-doc.component';
import { ViewUserAppsComponent } from './view-user-apps/view-user-apps.component';
import { CreateAppComponent} from './view-user-apps/createapp/createapp.component';
import { CreateAppService} from './view-user-apps/createapp/createapp.service';
import { ApplistComponent } from './view-user-apps/applist/applist.component';
import { MAService } from './view-user-apps/applist/applist.service';
import { NotfoundComponent } from './notfound/notfound.component';
import { routing } from './app-routing.module';

import { FaqService } from './faq/faq.service';
import { FilterPipe } from './faq/faqfilter.pipe'
import { EauthComponent } from './eauth/eauth.component';
import { ManageUserService } from './admin/platform/manageuser.service';
import { ManageUserComponent } from './admin/platform/manageuser.component';
import { AddPlatformComponent } from './admin/platform/add-platform/addplatform.component';
import { AddPlatformService } from './admin/platform/add-platform/addplatform.service';
import { EditPlatformComponent } from './admin/platform/edit-platform/editplatform.component';
import { EditPlatformService } from './admin/platform/edit-platform/editplatform.service';
import { DeletePlatformComponent } from './admin/platform/delete-platform/delete-platform.component';
import { DeleteProductComponent } from './admin/product/delete-product/delete-product.component';
import { DeleteProductService } from './admin/product/delete-product/delete-product.service';
import { AdminProductComponent } from './admin/product/add-product/addproduct.component';
import { AdminAddProductService } from './admin/product/add-product/addproduct.service';
import { AdminEditProductComponent } from './admin/product/edit-product/editproduct.component';
import { AdminEditProductService } from './admin/product/edit-product/editproduct.service';
//---------
import { SanitizeHtml } from './product/product-details/sanitizehtml.pipe';
import { KeysPipe } from './product/product-details/iterateMap.pipe';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { PublicProductComponent } from './product/product-list/public-product.component';
import { UserProductComponent } from './product/product-list/user-product.component';

import { MapPlatformProductComponent } from './admin/map-platform-product/mapplatformproduct.component';
import { MapPlatformProductService } from './admin/map-platform-product/mapplatformproduct.service';
import { OrgProdListFilterPipe } from './admin/map-platform-product/orgprodlist.filter.pipe';
import { MapFilterPipe } from './admin/map-platform-product/map-platform-product.filter.pipe';
import { ViewProductsComponent } from './admin/view-products/view-products.component';
import { ViewProductsService } from './admin/view-products/view-products.service';
import { ViewProductsPipe } from './admin/view-products/view-productsfilter.pipe'
import { ViewAllUsersComponent } from './admin/viewAllUsers/view-all-users.component';
import { ViewAllUsersService } from './admin/viewAllUsers/view-all-users.service';
import { ViewAllUsersPipe } from './admin/viewAllUsers/view-all-usersfilter.pipe';
import { ManageUserFilterPipe } from './admin/platform/manageuser-filter.pipe';
import { ProductFilterPipe } from './product/product-list/product-list-filter.pipe';

//Platform Admin module
import { PlatformAdminComponent } from './platform-admin/platform-admin.component';
import { PA_ManageUserComponent } from './platform-admin/manageUsers/pa-manageUsers.component';
import { PA_ManageUserService } from './platform-admin/manageUsers/pa-manageUsers.service';
import { PA_AddProductComponent } from './platform-admin/products/addProduct/pa-addProduct.component';
import { PA_AddProductService } from './platform-admin/products/addProduct/pa-addProduct.service';
import { PA_EditProductComponent } from './platform-admin/products/editProduct/pa-editProduct.component';
import { PA_EditProductService } from './platform-admin/products/editProduct/pa-editProduct.service';
import { PA_DeleteProductComponent } from './platform-admin/products/deleteProduct/pa-deleteProduct.component';
import { PA_DeleteProductService } from './platform-admin/products/deleteProduct/pa-deleteProduct.service';
import { PA_ManageUserFilterPipe } from './platform-admin/manageUsers/pa-manageUser-filter.pipe';

//External Admin
import { ExternalAdminComponent } from './ext-admin/ext-admin.component';

import { External_ManageUserComponent } from './ext-admin/manageUsers/ext-manageUsers.component';
import { External_ManageUserService } from './ext-admin/manageUsers/ext-manageUsers.service';
import { External_ManageUserFilterPipe } from './ext-admin/manageUsers/ext-manageUser-filter.pipe';

import { External_AddProductComponent } from './ext-admin/products/addProduct/ext-addProduct.component';
import { External_AddProductService } from './ext-admin/products/addProduct/ext-addProduct.service';
import { External_EditProductComponent } from './ext-admin/products/editProduct/ext-editProduct.component';
import { External_EditProductService } from './ext-admin/products/editProduct/ext-editProduct.service';
import { External_DeleteProductComponent } from './ext-admin/products/deleteProduct/ext-deleteProduct.component';
import { External_DeleteProductService } from './ext-admin/products/deleteProduct/ext-deleteProduct.service';

import { Public_AddProductComponent } from './ext-admin/public-products/addProduct/public-addProduct.component';
import { Public_AddProductService } from './ext-admin/public-products/addProduct/public-addProduct.service';
import { Public_EditProductComponent } from './ext-admin/public-products/editProduct/public-editProduct.component';
import { Public_EditProductService } from './ext-admin/public-products/editProduct/public-editProduct.service';

import { AddPartnerComponent } from './ext-admin/partner/add-partner/addpartner.component';
import { AddPartnerService } from './ext-admin/partner/add-partner/addpartner.service';
import { EditPartnerComponent } from './ext-admin/partner/edit-partner/editpartnercomponent';
import { EditPartnerService } from './ext-admin/partner/edit-partner/editpartner.service';
import { DeletePartnerComponent } from './ext-admin/partner/delete-partner/delete-partner.component';
import { DeletePartnerService } from './ext-admin/partner/delete-partner/delete-partner.service';
import { MapPartnerProductComponent } from './ext-admin/map-partner-product/mappartnerproduct.component';
import { MapPartnerProductService } from './ext-admin/map-partner-product/mappartnerproduct.service';
import { ProdOrgListFilterPipe} from './ext-admin/map-partner-product/prodorglist.filter.pipe';
import { MapPartnerFilterPipe} from './ext-admin/map-partner-product/map-partner-product.filter.pipe';
import { TokenPartnerComponent } from './ext-admin/partner-token/tokenpartner.component';
import { TokenPartnerService } from './ext-admin/partner-token/tokenpartner.service';

@NgModule({
  declarations: [
    AppComponent,
    FaqComponent,
    AmexHeaderComponent,
    AmexFooterComponent,
    AdminComponent,
    ProductComponent,
    ViewUserAppsComponent,
    ProdApiDocComponent,
    CreateAppComponent,
    ProductListComponent,
    ProductDetailComponent,
    FilterPipe,
    EauthComponent,
    ManageUserComponent,
    AddPlatformComponent,
    EditPlatformComponent,
    AdminProductComponent,
    DeletePlatformComponent,
    DeleteProductComponent,
    AdminEditProductComponent,
    ApiReferenceComponent,
    ViewAllUsersComponent,
    ViewAllUsersPipe,
    ApplistComponent,
    NotfoundComponent,
    ViewUserAppsComponent,
    CreateAppComponent,
    NotfoundComponent,
    //---------
    SanitizeHtml,
    KeysPipe,
    PublicProductComponent,
    UserProductComponent,
    OrderByPipe,

    MapPlatformProductComponent,
    OrgProdListFilterPipe,
    MapFilterPipe,
    ViewProductsComponent,
    ViewProductsPipe,
    ManageUserFilterPipe,
    ProductFilterPipe,
    PlatformAdminComponent,
    PA_ManageUserComponent,
    PA_AddProductComponent,
    PA_EditProductComponent,
    PA_DeleteProductComponent,
    PA_ManageUserFilterPipe,
    ExternalAdminComponent,
    External_ManageUserComponent,
    External_AddProductComponent,
    External_EditProductComponent,
    External_DeleteProductComponent,
    External_ManageUserFilterPipe,
    Public_AddProductComponent,
    Public_EditProductComponent,
    AddPartnerComponent,
    EditPartnerComponent,
    DeletePartnerComponent,
    MapPartnerProductComponent,
    ProdOrgListFilterPipe,
    MapPartnerFilterPipe,
    TokenPartnerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule,
    Ng2PageScrollModule.forRoot()
  ],

  // { provide: LocationStrategy, useClass: HashLocationStrategy },
  providers: [Title, ProductDetailService, ProductService, FaqService, MAService, CreateAppService, ViewAllUsersService, ManageUserService, AddPlatformService, EditPlatformService, AdminAddProductService, DeleteProductService, AdminEditProductService, MapPlatformProductService, ViewProductsService, AppService, PA_ManageUserService, PA_AddProductService, PA_EditProductService, PA_DeleteProductService, External_ManageUserService, External_AddProductService, External_EditProductService, External_DeleteProductService, Public_AddProductService, Public_EditProductService, AddPartnerService, EditPartnerService, MapPartnerProductService, DeletePartnerService, TokenPartnerService],

  bootstrap: [AppComponent]
})
export class AppModule { }
