import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Product} from '../product';
import {JsonpModule} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductDetailService{  
 // private baseUrl1 : string = "./app/product/product-details/UserManagementAPITR07.json";
  private baseUrl: string ='http://localhost:8085/dev-portal-intra/prodAPIDocumentation'; //"./app/product/product-details/productDetails.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl1: string ='http://localhost:8085/dev-portal-intra/updateDocumentation'; //"./app/product/product-details/productDetails.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl2: string ='http://localhost:8085/dev-portal-intra/prodAPIDocumentationExt'; //"./app/product/product-details/productDetails.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  public responseObj:JSON;
  private baseUrl5 : string = "http://localhost:8085/dev-portal-intra/resources.htm/"; //./app/product/product-details/UserManagementAPITR07.json";
 // private baseUrl5 : string = "./app/product/product-details/UserManagementAPITR07.json";

  constructor(private http : Http){
      this.responseObj = null;
  }

  // get product details

    getSwaggerJSON(prodName):Observable<JSON>{
       let headers = new Headers({ 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
       let params = new URLSearchParams();
       params.set('product_Name', prodName);
       return this.http.post(this.baseUrl5,params)
     // return this.http.get(this.baseUrl5)
        // ...and calling .json() on the response to return data
          .map((res:Response) => res.json())
          .catch((error:any) => Observable.throw('Server error'))
    }

    getPublicProductDetails (product_ID:string,product_name:string): Observable<JSON> {
        let bodyString = JSON.stringify({product_ID: product_ID, product_name: product_name}); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        console.log(bodyString);
         return this.http.post(this.baseUrl, bodyString, options) // ...using post request
                         .map((res:Response) => res.json())
                         .do(data => {
                           this.responseObj = data;
                         }) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any            
    }

    updateDocumentation(filename:string,productName:string,productId:string,dataToWrite:string,createUpdate:string): Observable<JSON>{
        //let bodyString = JSON.stringify({product_ID: product_ID, product_name: product_name}); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        //console.log(data);

        let params = new URLSearchParams();
        params.set('fileName', filename);
        params.set('productName', productName);
        params.set('productId', productId);
        params.set('dataToWrite', dataToWrite);
        params.set('createUpdate', createUpdate);
        
        return this.http.post(this.baseUrl1, params.toString() , options) // ...using post request
                         .map((res:Response) => res.json())
                         .do(data => {
                           this.responseObj = data;
                         }) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any
    }

      getUserProductDetails (product_ID:string,product_name:string,role:string,orgId:number): Observable<JSON> {
        let bodyString = JSON.stringify({product_ID: product_ID, product_name: product_name,roleName: role,orgId:orgId}); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        console.log(bodyString);
         return this.http.post(this.baseUrl2, bodyString, options) // ...using post request
                         .map((res:Response) => res.json())
                         .do(data => {
                           this.responseObj = data;
                         }) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any
                         
    }

}

