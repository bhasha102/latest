import { Component, EventEmitter, Input, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
//import { ProductStorage } from '../product-storage.service';
import { Product } from '../product';
import { ProductDetailService } from './product-detail.service';
import { SanitizeHtml } from './sanitizehtml.pipe';
import { KeysPipe } from './iterateMap.pipe';

@Component({
    selector: 'product-container',
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.less', '../../../assets/axp-dev-portal/css/common.css']
})

export class ProductDetailComponent implements OnInit {
    sub: any;
    documentId: string;
    documentName: string;
    documentOrgId: number;
    documentRoleName: string;
    productsDetails: any[];
    responseObj: JSON;
    leftNav: JSON;
    documentation: JSON = JSON.parse('[]');
    lastModifiedFile: string;
    responseTxt: string;
    highlightedDiv: number;
    isPublic: boolean;
    docData: string;
    showModal: boolean = false;
    private timer;
    ticks = 0;
    isDocLoaded = false;

    constructor(private route: ActivatedRoute, private _productDetailService: ProductDetailService, private router: Router) {
        this.isPublic = route.snapshot.data[0]['isPublic'];
        console.log("Public : " + this.isPublic);
    }

    ngOnInit() {

        this.sub = this.route.params.subscribe(params => {
            this.documentId = params['id'];            // + converts string to number
            this.documentName = params['name'];
            if (!this.isPublic) {
                if (params['orgId']) {
                    this.documentOrgId = +params['orgId'];
                }

                if (params['roleName']) {
                    this.documentRoleName = params['roleName'];
                }
            }
        });

        if (this.isPublic) {
            this.getPublicDetails();
        }
        else {
            this.getUserDetails();
        }
        //window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );      
    }

    getPublicDetails() {
        this._productDetailService.getPublicProductDetails(this.documentId, this.documentName).subscribe(
            response => {
                this.isDocLoaded = true;
                this.responseObj = response;
                this.documentation = this.responseObj['apiDocumentationData'];
                this.leftNav = this.responseObj['leftNavList'];
                this.lastModifiedFile = this.responseObj['lastModifiedFile'];
                /* if(window['CKEDITOR'].instances['prodAPIDoc'] == undefined)
                 {
                     window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );
                 }*/
            },
            error => { console.log(error) },
            () => {

                /*this.documentation = this.responseObj['_body']['apiDocumentationData'];
                this.leftNav = this.responseObj['_body']['leftNavList'];
                this.lastModifiedFile = this.responseObj['_body']['lastModifiedFile'];*/
            }
        );
    }

    getUserDetails() {
        // this._productDetailService.getUserProductDetails({id: this.documentId, name: this.documentName, roleId : this.documentRoleId, orgId : this.documentOrgId}).subscribe(
        //     response => this.responseObj = response,
        //     error => {console.log(error)}
        // );

        this._productDetailService.getUserProductDetails(this.documentId, this.documentName, this.documentRoleName, this.documentOrgId).subscribe(
            response => {
                this.responseObj = response;
                this.documentation = this.responseObj['apiDocumentationData'];
                this.leftNav = this.responseObj['leftNavList'];
                this.lastModifiedFile = this.responseObj['lastModifiedFile'];
                /* if(window['CKEDITOR'].instances['prodAPIDoc'] == undefined)
                 {
                     window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );
                 }*/
            },
            error => { console.log(error) },
            () => {

                /*this.documentation = this.responseObj['_body']['apiDocumentationData'];
                this.leftNav = this.responseObj['_body']['leftNavList'];
                this.lastModifiedFile = this.responseObj['_body']['lastModifiedFile'];*/
            }
        );
    }

    focusFunction() {
        console.log(window['CKEDITOR'].instances['prodAPIDoc']);
        if (window['CKEDITOR'].instances['prodAPIDoc'] == undefined) {
            window['CKEDITOR']['inline']('prodAPIDoc', { customConfig: '../../../assets/axp-dev-portal/ckeditor/config.js' });
        }
        // console.log("in focus");
        // console.log(window['CKEDITOR'].instances['prodAPIDoc']);
        //window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );
    }

    isActiveClass(index) {
        if (index == 0) {
            return "active";
        }
        return "";
    }

    addActiveClass(newValue: number) {
        if (this.highlightedDiv === newValue) {
            this.highlightedDiv = -1;
        }
        else {
            this.highlightedDiv = newValue;
        }
    }

    /* focusOutFunction(event)
     {
         console.log(event.target.id);
         let data = window['CKEDITOR'].instances['prodAPIDoc'].getData();
         //console.log(data);
         //console.log(window['CKEDITOR'].instances['prodAPIDoc']);
    }*/

    updateCurrentDoc() {

        this.docData = window['CKEDITOR'].instances['prodAPIDoc'] == undefined ? this.documentation : window['CKEDITOR'].instances['prodAPIDoc'].getData();
        /*Change the file name according to the dropdown selected*/
        //let stringData = "filename="+this.lastModifiedFile+"&productName="+this.documentName+"&productId="+this.documentId+"&dataToWrite="+this.docData+"&createUpdate=update";
        this._productDetailService.updateDocumentation(this.lastModifiedFile, this.documentName, this.documentId, this.docData, "update").subscribe(
            response => {
                this.responseObj = response;
                this.documentation = this.responseObj['apiDocumentation'];
                this.leftNav = this.responseObj['leftNav'];
                this.lastModifiedFile = this.responseObj['retFileName'];
                this.successMessage(true);
            },
            error => { console.log(error) }
        )

    }

    //Showing success message based on timer
    successMessage(isvisible) {
        this.timer = Observable.interval(1000).take(3);
        this.timer.subscribe(t => this.tickerFunc(t));
        this.showModal = isvisible;
    }

    tickerFunc(tick) {
        this.ticks = tick
        if (this.ticks >= 2) {
            this.showModal = false;
            this.ticks = 0;
        }
        return false;
    }

    navigateApiReference() {
        if (this.isPublic) {
            this.router.navigate(['/apiReference', this.documentId, this.documentName]);
        }
        else {
            this.router.navigate(['/apiReference', this.documentId, this.documentName, this.documentRoleName, this.documentOrgId]);
        }
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        if (window['CKEDITOR'].instances['prodAPIDoc'] != undefined) {
            window['CKEDITOR'].instances['prodAPIDoc'].destroy(true);
        }
    }
}
