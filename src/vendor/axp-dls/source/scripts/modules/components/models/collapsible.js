//
// Collapsible Model
//

import Model from '../../core/model';

export default class CollapsibleModel extends Model {}

CollapsibleModel.prototype.defaults = {
	closable: true,
	duration: 300
};
