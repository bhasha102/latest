//
// Dropdown Component
//

import View from '../../core/view';
import Component from '../classes/component';
import DropdownModel from '../models/dropdown.js';

//
// Configuration
//
const Name = 'Dropdown';
const Selector = '[data-toggle="dropdown"]:not([data-ignore])';
const DataKey = 'dls.dropdown';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class Dropdown extends Component {

	constructor (opts) {
		super(opts);
		this.component = DropdownView;

		this.render();
		this.onClickListen();
	}
}

//
// Dropdown View
//
class DropdownView extends View {

	constructor (opts) {
		super(opts);

		let container = DLS.util.request('format:selector:autoprefix',
				this.model.get('container')
			),
		 	menu = DLS.util.request('format:selector:autoprefix',
				this.model.get('menu')
			);

		this.container = container.substr(0, 1) === '#' ?
				$(container) :
				this.target.closest(container),
		this.menu = menu.substr(0, 1) === '#' ?
				$(menu) :
				this.container.find(menu);

		if(!this.container.length || !this.menu.length)
			throw new Error(`[DLS > ${Name} Error]: Could not find
				the 'container' and/or 'menu' element(s).`);
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Toggle
	//
	toggle() {
		if(!this.container.is('.open'))
			this.open()
		else
			this.close();
	}

	//
	// Open
	//
	open() {
		this.container.addClass('open');
		this.target.attr('aria-expanded', 'true');
		this.dropdownOpen();
	}

	//
	// Close
	//
	close() {
		this.container.removeClass('open');
		this.target.attr('aria-expanded', 'false');
		this.dropdownClose();
	}

	//
	// Dropdown opened
	//
	dropdownOpen() {
		$(document.body)
			.on(`click${EventKey}.${this.uid}`,
				$.proxy(DropdownView._confirmClick, this)
			);
	}

	//
	// Dropdown closed
	//
	dropdownClose() {
		$(document.body)
			.off(`click${EventKey}.${this.uid}`);
	}

	//
	// Confirm the click is our dropdown element
	//
	static _confirmClick(e) {
		if(!$.contains(this.container[0], e.target)) {
			if(this.container.is('.open'))
				this.close();
		}else {
			if($(e.target).prop('tagName').toLowerCase() === 'a' &&
				this.model.get('autoclose') &&
				this.container.is('.open'))
					this.close();
		}
	}

	//
	// Interface
	//
	static _interface(opts) {
		let $el = $(this),
			data = $el.data(DataKey),
			cmd = opts.invoke ? 'toggle' : $el.data(),
			config = Object.assign(
				{},
				$el.data(),
				typeof opts === 'object' && opts
			);

		if(!data) {
			data = new DropdownView({
				target: $el,
				model: new DropdownModel(config)
			});

			$el.data(DataKey, data);
		}

		if(typeof cmd === 'string')
			if(data[cmd] !== undefined)
				data[cmd]();

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
