//
// Tooltip Component
//

import Tether from 'tether';
import View from '../../core/view';
import Component from '../classes/component';
import TooltipModel from '../models/tooltip.js';
import TooltipTemplate from './templates/tmpl-tooltip.html!text';
import TooltipInfoTemplate from './templates/tmpl-tooltip-info.html!text';

//
// Configuration
//
const Name = 'Tooltip';
const Selector = '[data-toggle="tooltip"]:not([data-ignore])';
const DataKey = 'dls.tooltip';
const EventKey = `.${DataKey}`;
const Config = {
	tether: {
		map: {
			top: 'bottom center',
			right: 'middle left',
			bottom: 'top center',
			left: 'middle right'
		}
	}
}

//
// Initiator
//
export default class Tooltip extends Component {

	constructor (opts) {
		super(opts);
		this.component = TooltipView;

		this.render();
		this.onClickListen();
		this.onHoverListen();
		this.onFocusListen();
	}
}

//
// Tooltip View
//
class TooltipView extends View {

	constructor (opts) {
		super(opts);

		this.model.set('uid', Name + this.uid);
		this.triggers = this.model.get('trigger').split(' ');

		this.timeout = {};
		this.enabled = true;

		this.render();
		this.setProps();
		this.listen();
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Render
	//
	render() {
		let type = this.model.get('type'),
			template = type === 'info' ?
				TooltipInfoTemplate :
				TooltipTemplate;

		this.el = $(this.util.request('template:render',
			template,
			this.model.attributes
		));
	}

	//
	// Set the properties
	//
	setProps() {
		this.sel = {
			content: this.el.find('.tooltip-inner > span')
		};

		this.el.data(DataKey, this);
	}

	//
	// Listen
	//
	listen() {
		this.on('tether:position', this.refresh, this);

		this.model
			.on('change', this.update, this);

		this.app.vent
			.on(`component:${Name}:close:all`,
				$.proxy(this.onCloseAllTooltips, this)
			)
			.on(`component:Modal:open`,
				$.proxy(this.onCloseAllTooltips, this)
			);
	}

	//
	// Force all tooltips to close (e.g. - when a modal is opened)
	//
	onCloseAllTooltips() {
		setTimeout(() => {
			this.leave(true);
		}, 1);
	}

	//
	// Update the content of the tooltip from the model
	//
	update(e) {
		if(e.content === undefined)
			return;

		let content = this.model.get('content');

		this.sel.content
			.html(content);
	}

	//
	// Enter
	//
	enter(manual) {
		if(this.triggers.indexOf('hover') === -1 && !manual)
			return;

		clearTimeout(this.timeout.close);

		if(this.el.hasClass('in') || this.state === 'in') {
			this.state = 'in';
			return;
		}

		this.emit('enter', this);

		this.addActiveMonitor();

		this.state = 'in';

		clearTimeout(this.timeout.change);

		this.timeout.change = setTimeout( () => {
			if(this.state === 'in')
				this.show();
		}, this.model.get('delay') * 1000);
	}

	//
	// Leave
	//
	leave(manual) {
		if(this.triggers.indexOf('hover') === -1 && !manual || this.state === 'out')
			return;

		this.emit('leave', this);
		clearTimeout(this.timeout.change);

		this.removeActiveMonitor();

		this.state = 'out';
		this.timeout.change = setTimeout( () => {
				if(this.state === 'out')
					this.hide();
		}, this.model.get('delay') * 1000);
	}

	//
	// Toggle
	//
	toggle(e) {
		if(this.el.hasClass('in'))
			this.leave(true);
		else
			this.enter(true);
	}

	//
	// Show the tooltip
	//
	show(manual) {
		this.emit('showing', this);
		clearTimeout(this.timeout.close);

		if(manual)
			this.manual = true;

		let contains = $.contains(
			this.target[0].ownerDocument.documentElement,
			this.target[0]
		);

		if(!this.enabled || !contains)
			return this;

		this.el.appendTo(document.body);
		this.target.attr('aria-describedby', this.model.get('uid'));

		this.addTether();
		this.fadeInTooltip();
		this.tooltipListen();
	}

	//
	// Hide
	//
	hide(manual) {
		this.emit('hiding', this);
		clearTimeout(this.timeout.close);

		if(manual)
			this.manual = false;

		if(this.manual && !manual)
			return;

		this.fadeOutTooltip();
	}

	//
	// Fade in tooltip
	//
	fadeInTooltip() {
		this.el.addClass('in');

		clearTimeout(this.timeout.fade);
		this.timeout.fade = setTimeout($.proxy(this.onShown, this), 250);
		if(!this.model.get('animation') === '')
			this.onShown();
	}

	//
	// Fade out tooltip
	//
	fadeOutTooltip() {
		this.el.removeClass('in');
		clearTimeout(this.timeout.fade);

		if(this.model.get('animation') !== '')
			this.timeout.fade = setTimeout(
				$.proxy(this.onHidden, this),
				250
			);
		else
			this.onHidden();
	}

	//
	// Bind temporary listeners (remove when the element is removed from DOM)
	//
	tooltipListen() {
		this.el
			.on(`click${EventKey}`, '.tooltip-close', (e) => {
				this.toggle();
				e.preventDefault();
			});
	}

	//
	// On Shown
	//
	onShown() {
		clearTimeout(this.timeout.fade);
		let prevState = this.state;
		this.state = null;

		this.emit('shown');

		if(prevState === 'out')
			this.leave(null);
	}

	//
	// On Hidden
	//
	onHidden() {
		clearTimeout(this.timeout.fade);

		if(this.state !== 'in' && this.el.parent().length) {
			this.el.remove();
			this.target.removeAttr('aria-describedby');

			this.untether();

			this.emit('hidden');
		}
	}

	//
	// Add active state monitor
	//
	addActiveMonitor() {
		let uid = this.model.get('uid');

		clearTimeout(this.timeout.monitor);
		this.timeout.monitor = setTimeout( () => {
			$(document.body)
				.on(`${this.platform.event.down}${EventKey}.${uid}`,
					$.proxy(this.confirmClick, this)
				);
		}, 10);

		$(window)
			.on(`resize${EventKey}.${uid}`,
				$.proxy(this.refresh, this)
			);
	}

	//
	// Remove active state monitor
	//
	removeActiveMonitor() {
		let uid = this.model.get('uid');

		clearTimeout(this.timeout.monitor);

		$(document.body)
			.off(`click${EventKey}.${uid}`,
				$.proxy(this.confirmClick, this)
			);

		$(window)
			.off(`resize${EventKey}.${uid}`,
				$.proxy(this.refresh, this)
			);
	}

	//
	// Delayed Hide
	//
	delayedHide(time) {
		clearTimeout(this.timeout.close);
		this.timeout.close = setTimeout( () => {
			this.hide(true);
		}, time);
	}

	//
	// Confirm Click
	//
	confirmClick(e) {
		if(!$.contains(this.el[0], e.target)) {
			if(this.el.hasClass('in')) {
				this.leave(true);
			}
		}
	}

	//
	// Refresh the tether
	//
	refresh() {
		if(this.tether)
			this.tether.position();
	}

	//
	// Add tether
	//
	addTether() {
		let attachment = this.attachment( this.model.get('placement') );

		this.untether();
		this.tether = new Tether({
			attachment,
			element: this.el,
			target: this.target,
			offset: this.model.get('offset'),
			constraints: this.model.get('constraints'),
			classPrefix: 'tooltip'
		});

		this.app.vent.trigger('resize:trigger');
		this.tether.position();

		// Mini throttle tether position to ensure it is placed correctly
		setTimeout( () => {
			if(this.tether)
				this.tether.position();
		}, 10);
	}

	//
	// Untether
	//
	untether() {
		if(this.tether)
			this.tether.destroy();
	}

	//
	// Get attachment position
	//
	attachment(placement) {
		return Config.tether.map[placement] || 'bottom center';
	}



	//
	// Interface
	//
	static _interface(opts) {
		let $el = this,
			data = $el.data(DataKey),
			cmd = $el.data(),
			config = Object.assign(
				{},
				$el.data(),
				typeof opts === 'object' && opts
			);

		if(opts.invoke) {
			let invokeState = opts.invokeEnter ? 'enter' : 'leave';

			if(opts.invokeHover || opts.invokeFocus) {
				cmd = `${invokeState}`;
			} else {
				cmd = 'toggle';
			}
		}

		if(!data) {
			let title = $el.attr('title');

			if(config.type === 'info' && !config.theme)
				config.theme = 'white';

			if(title && config.content === undefined) {
				config.originalTitle = title;
				config.content = title;
				$el.removeAttr('title');
			}

			data = new TooltipView({
				target: $el,
				model: new TooltipModel(config)
			});

			$el.data(DataKey, data);
		}

		if(typeof cmd === 'string')
			if(data[cmd] !== undefined) {
				if(data.get('trigger').indexOf('click') === -1 && cmd === 'toggle')
					return;

				data[cmd]();
			}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		let originalTitle = this.model.get('originalTitle');
		clearTimeout(this.timeout.close);
		clearTimeout(this.timeout.change);
		clearTimeout(this.timeout.fade);

		this.removeActiveMonitor();
		this.untether();

		if(this.el)
			this.el.remove();

		if(originalTitle != null)
			this.target.attr('title', originalTitle);

		$.removeData(this.target, DataKey);
	}
}
