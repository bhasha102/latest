//
// Gulp Config
//
// Our core configuration for gulp.

'use strict';

module.exports = (function (config, argv) {
	config.pkg = require('./package.json');
	// Pass command line arguments
	config.argv = argv;

	// Host name
	config.host = argv.host ? argv.host : 'localhost';

	// Server port (e.g. Override with --port=[PORT])
	config.port = argv.port && (argv.port % 1 === 0) ? argv.port : 8000;

	config.preprocess = {
		VERSION: config.pkg.version
	}

	// Directories
	config.dirs = {
		root: __dirname,
		src: 'source',
		dist:'dist'
	};

	// BrowserSync
	config.browserSync = {
		options: {
			logSnippet: false,
			notify: false,
			ghostMode: false
		},

		instance: argv._[0] != 'docs' ? null : require('browser-sync').create()
	};

	// Styles
	config.styles = {
		src: config.dirs.src + '/styles/**/*.scss',
		dest: config.dirs.dist + '/styles'
	};

	// Scripts
	config.scripts = {
		startScriptPath: config.dirs.src + '/scripts/main',
		src: config.dirs.src + '/scripts/**/*.js',
		dest: config.dirs.dist + '/scripts',
		exportName: 'dls'
	};

	// Iconfonts
	config.iconfont = {
		src: config.dirs.src + '/iconfont/**/*.svg',
		scss: {
			template: config.dirs.src + '/styles/_templates/_iconfont-template.scss',
			output: config.dirs.src +'/styles/modules/style',
			filename: '_iconfont',
			fontRelPath: '../iconfont/'
		},
		dest: config.dirs.dist + '/iconfont',
		svg: {
			dest: config.dirs.dist + '/img/svg/icon',
			relPath: '../img/svg/icon'
		},
		name: 'dls-icons'
	}

	// Clean
	config.clean = [
		config.dirs.dist + '/**/*',
		'!.gitkeep'
	];

	config.semver = {

	};


	return config;

})({}, require('yargs').argv);
