function initPortalHome() {

	
	var windowW = window.innerWidth,
	$window = $(window),
	$body = $('body'),
	isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
	scrollTop = getScrollTop();

	window.requestAnimFrame = (function(){
		return  window.requestAnimationFrame       ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame    ||
		window.oRequestAnimationFrame      ||
		window.msRequestAnimationFrame     ||
		function(/* function */ callback, /* DOMElement */ element){
			window.setTimeout(callback, 1000 / 60);
		};
	})();	

	function loadComplete(argument) {

		waypoints.charge();
		
		setTimeout(function() {
			$body.addClass('loaded').one('transitionend',function() {
				$('.hero-copy > *').addClass('in');
			})

			if(isMobile) {
				console.log('mobile');
			}

		}, 600)

	};
	
	var textRotate = (function() {


		function init() {
			/*! Blast.js (2.0.0): julian.com/research/blast (C) 2015 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */
			!function($,e,t,a){var r=function(){if(t.documentMode)return t.documentMode;for(var e=7;e>0;e--){var r=t.createElement("div");if(r.innerHTML="<!--[if IE "+e+"]><span></span><![endif]-->",r.getElementsByTagName("span").length)return r=null,e;r=null}return a}(),n=e.console||{log:function(){},time:function(){}},i="blast",s={latinPunctuation:"–—′’'“″„\"(«.…¡¿′’'”″“\")».…!?",latinLetters:"\\u0041-\\u005A\\u0061-\\u007A\\u00C0-\\u017F\\u0100-\\u01FF\\u0180-\\u027F"},l={abbreviations:new RegExp("[^"+s.latinLetters+"](e\\.g\\.)|(i\\.e\\.)|(mr\\.)|(mrs\\.)|(ms\\.)|(dr\\.)|(prof\\.)|(esq\\.)|(sr\\.)|(jr\\.)[^"+s.latinLetters+"]","ig"),innerWordPeriod:new RegExp("["+s.latinLetters+"].["+s.latinLetters+"]","ig"),onlyContainsPunctuation:new RegExp("[^"+s.latinPunctuation+"]"),adjoinedPunctuation:new RegExp("^["+s.latinPunctuation+"]+|["+s.latinPunctuation+"]+$","g"),skippedElements:/(script|style|select|textarea)/i,hasPluginClass:new RegExp("(^| )"+i+"( |$)","gi")};$.fn[i]=function(d){function o(e){return e.replace(l.abbreviations,function(e){return e.replace(/\./g,"{{46}}")}).replace(l.innerWordPeriod,function(e){return e.replace(/\./g,"{{46}}")})}function c(e){return e.replace(/{{(\d{1,3})}}/g,function(e,t){return String.fromCharCode(t)})}function u(e,a){var r=t.createElement(a.tag);if(r.className=i,a.customClass&&(r.className+=" "+a.customClass,a.generateIndexID&&(r.id=a.customClass+"-"+f.blastedIndex)),"all"===a.delimiter&&/\s/.test(e.data)&&(r.style.whiteSpace="pre-line"),a.generateValueClass===!0&&!a.search&&("character"===a.delimiter||"word"===a.delimiter)){var n,s=e.data;"word"===a.delimiter&&l.onlyContainsPunctuation.test(s)&&(s=s.replace(l.adjoinedPunctuation,"")),n=i+"-"+a.delimiter.toLowerCase()+"-"+s.toLowerCase(),r.className+=" "+n}return a.aria&&r.setAttribute("aria-hidden","true"),r.appendChild(e.cloneNode(!1)),r}function g(e,t){var a=-1,r=0;if(3===e.nodeType&&e.data.length){if(f.nodeBeginning&&(e.data=t.search||"sentence"!==t.delimiter?c(e.data):o(e.data),f.nodeBeginning=!1),a=e.data.search(h),-1!==a){var n=e.data.match(h),i=n[0],s=n[1]||!1;""===i?a++:s&&s!==i&&(a+=i.indexOf(s),i=s);var d=e.splitText(a);d.splitText(i.length),r=1,t.search||"sentence"!==t.delimiter||(d.data=c(d.data));var p=u(d,t,f.blastedIndex);d.parentNode.replaceChild(p,d),f.wrappers.push(p),f.blastedIndex++}}else if(1===e.nodeType&&e.hasChildNodes()&&!l.skippedElements.test(e.tagName)&&!l.hasPluginClass.test(e.className))for(var m=0;m<e.childNodes.length;m++)f.nodeBeginning=!0,m+=g(e.childNodes[m],t);return r}function p(t,s){s.debug&&n.time("blast reversal");var l=!1;t.removeClass(i+"-root").removeAttr("aria-label").find("."+i).each(function(){var e=$(this);if(e.closest("."+i+"-root").length)l=!0;else{var t=this.parentNode;7>=r&&t.firstChild.nodeName,t.replaceChild(this.firstChild,this),t.normalize()}}),e.Zepto?t.data(i,a):t.removeData(i),s.debug&&(n.log(i+": Reversed Blast"+(t.attr("id")?" on #"+t.attr("id")+".":".")+(l?" Skipped reversal on the children of one or more descendant root elements.":"")),n.timeEnd("blast reversal"))}var m=$.extend({},$.fn[i].defaults,d),h,f={};if(m.search.length&&("string"==typeof m.search||/^\d/.test(parseFloat(m.search))))m.delimiter=m.search.toString().replace(/[-[\]{,}(.)*+?|^$\\\/]/g,"\\$&"),h=new RegExp("(?:^|[^-"+s.latinLetters+"])("+m.delimiter+"('s)?)(?![-"+s.latinLetters+"])","i");else switch("string"==typeof m.delimiter&&(m.delimiter=m.delimiter.toLowerCase()),m.delimiter){case"all":h=/(.)/;break;case"letter":case"char":case"character":h=/(\S)/;break;case"word":h=/\s*(\S+)\s*/;break;case"sentence":h=/(?=\S)(([.]{2,})?[^!?]+?([.…!?]+|(?=\s+$)|$)(\s*[′’'”″“")»]+)*)/;break;case"element":h=/(?=\S)([\S\s]*\S)/;break;default:if(!(m.delimiter instanceof RegExp))return n.log(i+": Unrecognized delimiter, empty search string, or invalid custom Regex. Aborting."),!0;h=m.delimiter}if(this.each(function(){var e=$(this),r=e.text();if(d!==!1){f={blastedIndex:0,nodeBeginning:!1,wrappers:f.wrappers||[]},e.data(i)===a||"search"===e.data(i)&&m.search!==!1||(p(e,m),m.debug&&n.log(i+": Removed element's existing Blast call.")),e.data(i,m.search!==!1?"search":m.delimiter),m.aria&&e.attr("aria-label",r),m.stripHTMLTags&&e.html(r);try{t.createElement(m.tag)}catch(s){m.tag="span",m.debug&&n.log(i+": Invalid tag supplied. Defaulting to span.")}e.addClass(i+"-root"),m.debug&&n.time(i),g(this,m),m.debug&&n.timeEnd(i)}else d===!1&&e.data(i)!==a&&p(e,m);m.debug&&$.each(f.wrappers,function(e,t){n.log(i+" ["+m.delimiter+"] "+this.outerHTML),this.style.backgroundColor=e%2?"#f12185":"#075d9a"})}),d!==!1&&m.returnGenerated===!0){var b=$().add(f.wrappers);return b.prevObject=this,b.context=this.context,b}return this},$.fn.blast.defaults={returnGenerated:!0,delimiter:"word",tag:"span",search:!1,customClass:"",generateIndexID:!1,generateValueClass:!1,stripHTMLTags:!1,aria:!0,debug:!1}}(window.jQuery||window.Zepto,window,document);

			$('.hero-header').blast({
				delimiter: 'letter'
			}).css({
				'opacity': 0, 
				'display': 'inline-block'
			})

			




		}  
		return { 
			init:init   
		};
	})(); 


	function globals() { 
		windowW = window.innerWidth;

		$(window).on('resize', function() {
			windowW = window.innerWidth;
			
			if(!isMobile) {
				$('.parallax').removeAttr('style');
				$.stellar('refresh');
			}

		});

		$window.on('scroll', function() {
			scrollTop = getScrollTop(); 
	    });

		$('.api').hover(
			function () {
				var imgSrc = $(this).find('.api__active').data('img');
				$(this).find('.api__active img').attr('src', imgSrc);
			},
			function() {
				$(this).find('.api__active img').attr('src', '');
			}
			);


		if(!isMobile) {
			$('.parallax').attr({
				'data-stellar-offset-parent'    :true
			});
			$.stellar({
				responsive: false,
				horizontalScrolling: false,
				hideDistantElements: false,
				positionProperty: 'transform',
				scrollProperty: 'scroll',
				verticalOffset: 0
			});
		}


	};

	function getScrollTop(){
		if(typeof pageYOffset !== 'undefined'){
            //most browsers except IE before #9
            return pageYOffset;
        }
        else{
            var B= document.body; //IE 'quirks'
            var D= document.documentElement; //IE with doctype
            D= (D.clientHeight)? D: B;
            return D.scrollTop;
        }
    }

	var waypoints = (function() {

		var item,
		items = [
		'.hero-copy > *',
		'#icon-list-input > li',
		'.api-copy > *',
		'.cloud-copy > *',
		'.section-handler',
		'.api-collection',
		'.device',
		'.homepage-footer'
		],
		weakItems = [];

		function init() {


			for(var i = 0; i < items.length; i++) {
				$(items[i]).addClass('out');
			}
			item = $('.out');


		}

		function charge() {
			item.each(function() {
				actionHandler(this);
			});

		}

		function actionHandler(el) {

			var waypoint = new Waypoint({
				element: el,
				handler: function(direction) {
					var $el = $(this.element);
					var delay = $el.index() * 50;

					if(direction === 'down') {

						if( $el.closest('.hero-copy').length) {
							delay = $el.index() * 100 + 4000;
						}
						if( $el.closest('.hero-devices').length) {
							delay = $el.index() * 100 + 1000;
						}

						setTimeout(function (){
							$el.addClass('in');

							$('.blast').each(function(index, el) {
								var that = $(el);
								setTimeout(function () {
									that.css('opacity', 1);
								}, index * 100);
								
							});
						}, delay);


					}
					if(direction === 'up') {
						$el.removeClass('in');
					}
				},
				offset: "90%"
			});
		}

		return {
			init: init,
			actionHandler:actionHandler,
			charge: charge
		};
	})();

	var deviceCarousel = (function() {
		var run = false,
		swiper;

		function init() {
			if(windowW < 768 && !run) {
				swiper = new Swiper('.swiper-container', {
					effect: 'coverflow',
					grabCursor: true,
					centeredSlides: true,
					slidesPerView: 'auto',
					initialSlide: 1,
					// loop: true,
					autoplay: 7000,
					coverflow: {
						rotate: 50,
						stretch: 0,
						depth: 100,
						modifier: 1,
						slideShadows: false
					}
				})
				run = true;
			}


		}
		$window.on('resize',function() {
			if(windowW >= 768 && run) {
				swiper.destroy();
				$('.swiper-container, .swiper-wrapper, .swiper-slide').removeAttr('style')
				run = false;
			}
			if( windowW < 768 && !run) {
				deviceCarousel.init();
			}
		});

		return {
			init :init
		}
	})();

	var constellation = (function() {

		function init() {
			var starCount = 0;
			var starLineDistance = 0;
			var starWidth = 0;
			if(windowW < 768) {
				starCount = 20;
				starLineDistance = 100;
				starWidth = 3;
			}
			if(windowW >= 768) {
				starCount = 50;
				starLineDistance = 200;
				starWidth = 6;
			}

			$('#hero-canvas').constellation({
				star: {
					color: 'rgba(48, 78, 128, .8)',
					width: starWidth
				},
				line: {
					color: 'rgba(48, 78, 128, .4)'
				},
				radius: 2500,
				starCount: starCount,
				distance: starLineDistance,
				height: window.innerHeight
			});
		}

		$window.on('resize',function() {
			windowW = window.innerWidth;
			constellation.init();
		});

		return {
			init :init
		}

	})();

	function globalInit(callback) {
		globals();

		waypoints.init();
		textRotate.init();
		deviceCarousel.init();
		constellation.init();

		if(callback) {
			callback();
		}
	}

	globalInit(function () {
		
		loadComplete();

	});

	//Nav-SideBar
    function sticky_relocate() {
        var window_top = $(window).scrollTop() + 100;
        var stickyanchor =$('#sticky-anchor');
        if(stickyanchor.length) {
            var div_top = $('#sticky-anchor').offset().top;

            if (window_top > div_top) {

                $('#sticky_nav').addClass('stick');
                $('#sticky-anchor').height($('#sticky_nav').outerHeight());


            } else {
                $('#sticky_nav').removeClass('stick');
                $('#sticky-anchor').height(0);

            }
        }
    }

    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });
}




